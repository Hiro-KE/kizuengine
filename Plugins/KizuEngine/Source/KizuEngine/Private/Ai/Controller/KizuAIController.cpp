// Fill out your copyright notice in the Description page of Project Settings.

#include "Ai/Controller/KizuAIController.h"
#include "Engine/Engine.h"
#include "Core/KizuCharacter.h"
#include "FunctionLibrary/KizuGlobalFunctionLibrary.h"

UPROPERTY()
AKizuCharacter* CharacterRef;

AKizuAIController::AKizuAIController()
{
	bAiDataHasBeenSet = false;
	// Setup the perception component
	//PerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerception Component"));
	//SightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Config"));
	//PerceptionComponent->ConfigureSense(*SightConfig);
	//PerceptionComponent->SetDominantSense(SightConfig->GetSenseImplementation());
	//PerceptionComponent->OnPerceptionUpdated.AddDynamic(this, &AKizuAIController::PerceptionSenseUpdate);
}

// Called when the game starts or when spawned
void AKizuAIController::BeginPlay()
{
	Super::BeginPlay();
	//PerceptionComponent->SetActive(true);

}




void AKizuAIController::SetAiData(UAiDataAsset* AiData)
{
	if (AiData) {
		if (AiData->BehaviorTree){

			this->RunBehaviorTree(AiData->BehaviorTree);
			if (GetBrainComponent()) {
				this->GetBrainComponent()->RestartLogic();
				this->GetBrainComponent()->Activate(true);
				
			}
			UKizuGlobalFunctionLibrary::KizuMessage("Behavior Tree is Running for " + GetName());
			//SightConfig->SightRadius = AiData->SightRadius;
			//SightConfig->LoseSightRadius = AiData->LoseSightRadius;
			//SightConfig->PeripheralVisionAngleDegrees = AiData->VisionAngleDegree;
			//bAiDataHasBeenSet = true;
			//SightConfig->DetectionByAffiliation.bDetectEnemies = true;
			//SightConfig->DetectionByAffiliation.bDetectNeutrals = true;
			//SightConfig->DetectionByAffiliation.bDetectFriendlies = true;
			//PerceptionComponent->ConfigureSense(*SightConfig);

		}
	}
}

void AKizuAIController::SetAiData()
{
		/*SightConfig->SightRadius = 1000.f;
		SightConfig->LoseSightRadius = 1200.f;
		SightConfig->PeripheralVisionAngleDegrees = 200.f;
		SightConfig->DetectionByAffiliation.bDetectEnemies = true;
		SightConfig->DetectionByAffiliation.bDetectNeutrals = true;
		SightConfig->DetectionByAffiliation.bDetectFriendlies = true;
		PerceptionComponent->ConfigureSense(*SightConfig);*/
}

void AKizuAIController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	/*CharacterRef = Cast<AKizuCharacter>(InPawn);
	if (CharacterRef) {
		UKizuGlobalFunctionLibrary::KizuMessage("Character  [" + CharacterRef->GetName() + "] has been Possessed by [" + this->GetName() + "]");
		UAiDataAsset* AiData = CharacterRef->CharacterDataAsset->AiDataAsset;
		if (AiData) {
			if (AiData->ResetOnEachPossess || !bAiDataHasBeenSet)
			{
				SetAiData(AiData);
			}
		}
		else SetAiData();

	}*/
	
}


