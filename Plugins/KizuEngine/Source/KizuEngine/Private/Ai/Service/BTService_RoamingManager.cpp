// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_RoamingManager.h"
#include "BehaviorTree/BTFunctionLibrary.h"
#include "NavigationSystem/Public/NavigationSystem.h"
#include "BehaviorTree/BlackboardComponent.h"


void UBTService_RoamingManager::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
	if (OwnerComp.IsValidLowLevel()) {
		if(!CurrentActor) CurrentActor = Cast<AActor>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(ActorToRoamAround.SelectedKeyName));
		if (CurrentActor) {
			UNavigationSystemV1::K2_GetRandomReachablePointInRadius(CurrentActor, CurrentActor->GetActorLocation(), TempVector, RoamingRadius);
			OwnerComp.GetBlackboardComponent()->SetValueAsVector(NewLocation.SelectedKeyName, TempVector);
		}
	}
}
