// Fill out your copyright notice in the Description page of Project Settings.


#include "Ai/Service/BTService_SensedActors.h"
#include "BehaviorTree/BTFunctionLibrary.h"
#include "FunctionLibrary/KizuGlobalFunctionLibrary.h"
#include "BehaviorTree/BlackboardComponent.h"

void UBTService_SensedActors::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
	if (OwnerComp.IsValidLowLevel()) {
		SetReferences(&OwnerComp);
		if (CharacterRef->PawnSensed && SensedPawn != CharacterRef->PawnSensed) {
			UKizuGlobalFunctionLibrary::KizuMessage("<Service - SensedActor> [" + CharacterRef->GetName() + "] Updating Sensed actor to [" + CharacterRef->PawnSensed->GetName() + "]");
			SensedPawn = CharacterRef->PawnSensed;
		}
		OwnerComp.GetBlackboardComponent()->SetValueAsObject(SensedActorsKey.SelectedKeyName, SensedPawn);
	}
}