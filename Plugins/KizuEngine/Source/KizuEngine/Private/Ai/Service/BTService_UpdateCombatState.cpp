// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_UpdateCombatState.h"
#include "BehaviorTree/BTFunctionLibrary.h"
#include "BehaviorTree/BlackboardComponent.h"

void UBTService_UpdateCombatState::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
	if (OwnerComp.IsValidLowLevel()) {
		if (CombatComponent->IsValidLowLevel())
			OwnerComp.GetBlackboardComponent()->SetValueAsEnum(CombatState.SelectedKeyName, CombatComponent->GetCombatState());
	}
}
