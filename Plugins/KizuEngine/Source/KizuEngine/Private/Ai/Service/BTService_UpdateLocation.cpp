// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_UpdateLocation.h"
#include "BehaviorTree/BTFunctionLibrary.h"
#include "BehaviorTree/BlackboardComponent.h"

void UBTService_UpdateLocation::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
	if (OwnerComp.IsValidLowLevel()) {
		TempActor = Cast<AActor>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(TargetActor.SelectedKeyName));
		if (TempActor)
			OwnerComp.GetBlackboardComponent()->SetValueAsVector(LocationVector.SelectedKeyName, TempActor->GetActorLocation());
	}
}
