// Fill out your copyright notice in the Description page of Project Settings.


#include "Ai/Service/KizuBTService.h"
#include "FunctionLibrary/KizuGlobalFunctionLibrary.h"



UKizuBTService::UKizuBTService()
{
	bDataHasBeenSet = false;
	bCreateNodeInstance = true;
	bNotifyBecomeRelevant = true;
}

void UKizuBTService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
	if (AiControllerRef)
		OnTick(AiControllerRef, DeltaSeconds);
}

void UKizuBTService::OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::OnBecomeRelevant(OwnerComp, NodeMemory);
	SetReferences(&OwnerComp);
}

void UKizuBTService::SetReferences(UBehaviorTreeComponent* OwnerComp)
{
	AiControllerRef = Cast<AKizuAIController>(OwnerComp->GetOwner());
	if (AiControllerRef) {
		CharacterRef = Cast<AKizuCharacter>(AiControllerRef->GetPawn());
		if (CharacterRef) {
			CombatComponent = UKizuCombatFunctionLibrary::GetCombat(CharacterRef);
			InventoryComponent = UKizuInventoryFunctionLibrary::GetInventoryComponent(CharacterRef);
			bDataHasBeenSet = true;
		}
	}
}


