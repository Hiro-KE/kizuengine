// Fill out your copyright notice in the Description page of Project Settings.

#include "BTTask_UseAbility.h"
#include "KizuGlobalFunctionLibrary.h"


EBTNodeResult::Type UBTTask_UseAbility::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);
	if (CombatComponent){
		CombatComponent->PlayAbility(AbilityName);
		UKizuGlobalFunctionLibrary::KizuMessage(" <BehaviorTree> [" + CharacterRef->GetName() + "] successfully used the Ability [" + AbilityName.ToString() + "]");
		return EBTNodeResult::Succeeded;
	}
	else {
		UKizuGlobalFunctionLibrary::KizuMessage(" <BehaviorTree> [" + CharacterRef->GetName() + "] could not use the Ability [" + AbilityName.ToString() + "]");
		return EBTNodeResult::Failed;
	}
}
