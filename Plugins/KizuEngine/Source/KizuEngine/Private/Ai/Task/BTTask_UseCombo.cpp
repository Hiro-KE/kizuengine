// Fill out your copyright notice in the Description page of Project Settings.

#include "BTTask_UseCombo.h"
#include "KizuGlobalFunctionLibrary.h"


EBTNodeResult::Type UBTTask_UseCombo::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);
	if (CombatComponent) {
		CombatComponent->PlayCombo();
		UKizuGlobalFunctionLibrary::KizuMessage(" <BehaviorTree> [" + CharacterRef->GetName() + "] successfully used a combo");
		return EBTNodeResult::Succeeded;
	}
	else {
		UKizuGlobalFunctionLibrary::KizuMessage(" <BehaviorTree> [" + CharacterRef->GetName() + "] could not use a combo");
		return EBTNodeResult::Failed;
	}
}
