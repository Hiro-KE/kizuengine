// Fill out your copyright notice in the Description page of Project Settings.

#include "Ai/Task/KizuBTTask.h"
#include "KizuGlobalFunctionLibrary.h"

UKizuBTTask::UKizuBTTask()
{
	bDataHasBeenSet = false;
	bCreateNodeInstance = true;
}


void UKizuBTTask::SetReferences(UBehaviorTreeComponent* OwnerComp)
{
		AiControllerRef = Cast<AKizuAIController>(OwnerComp->GetOwner());
		CharacterRef = AiControllerRef ? Cast<AKizuCharacter>(AiControllerRef->GetPawn()) : NULL;
		CombatComponent = UKizuCombatFunctionLibrary::GetCombat(CharacterRef);
		InventoryComponent = UKizuInventoryFunctionLibrary::GetInventoryComponent(CharacterRef);
		bDataHasBeenSet = true;
}

EBTNodeResult::Type UKizuBTTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);
	SetReferences(&OwnerComp);
	if (AiControllerRef) {
		OnExecute(AiControllerRef);
		return EBTNodeResult::Succeeded;
	}
	return EBTNodeResult::Failed;
}

