// Fill out your copyright notice in the Description page of Project Settings.

#include "AnimNotifyState_MeleeDamage.h"
#include "KizuCombatFunctionLibrary.h"

UAnimNotifyState_MeleeDamage::UAnimNotifyState_MeleeDamage() {
	DebugLine = false;
}

void UAnimNotifyState_MeleeDamage::NotifyBegin(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation, float TotalDuration)
{
	if (MeshComp)
		if (MeshComp->GetOwner())
			 CombatComp = UKizuCombatFunctionLibrary::GetCombat(MeshComp->GetOwner());
}

void UAnimNotifyState_MeleeDamage::NotifyTick(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation, float FrameDeltaTime)
{
	if(CombatComp) CombatComp->TraceAndDamageLoop(TraceStart, TraceEnd, Width, DebugLine);
}

void UAnimNotifyState_MeleeDamage::NotifyEnd(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation)
{
	if (CombatComp) CombatComp->TraceAndDamageEnd();
}
