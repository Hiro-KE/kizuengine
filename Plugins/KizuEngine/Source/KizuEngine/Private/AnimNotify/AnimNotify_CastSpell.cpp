// Fill out your copyright notice in the Description page of Project Settings.

#include "AnimNotify_CastSpell.h"
#include "KizuCombatFunctionLibrary.h"
void  UAnimNotify_CastSpell::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	if (MeshComp)
	{
		if (MeshComp->GetOwner()) {
			UKizuCombatComponent* CombatComp = UKizuCombatFunctionLibrary::GetCombat(MeshComp->GetOwner());

			if (CombatComp)
				CombatComp->CastSpell(SpellName);
		}
	}
}