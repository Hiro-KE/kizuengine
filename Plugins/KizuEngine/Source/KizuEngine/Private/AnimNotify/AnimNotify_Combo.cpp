// Fill out your copyright notice in the Description page of Project Settings.

#include "AnimNotify_Combo.h"
#include "KizuCombatFunctionLibrary.h"
#include "KizuGlobalFunctionLibrary.h"
#include "KizuEngine.h"

void  UAnimNotify_Combo::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	if (MeshComp)
	{
		if (MeshComp->GetOwner()) {
			UKizuCombatComponent* CombatComp = UKizuCombatFunctionLibrary::GetCombat(MeshComp->GetOwner());

			if (CombatComp)
				UKizuCombatFunctionLibrary::ComboCounter(CombatComp, ComboState);
		}
	}
}

