// Fill out your copyright notice in the Description page of Project Settings.

#include "KizuCombatComponent.h"
#include  "KizuGlobalFunctionLibrary.h"
#include "KizuCombatFunctionLibrary.h"
#include "FunctionLibrary/ObjectivesFunctionLibrary.h"
#include  "Engine.h"
#include "Engine/Engine.h"
#include "Net/UnrealNetwork.h"
#include "KizuWeapon.h"
#include "DataAsset/AbilityObjectivesDataAsset.h"
#include "DrawDebugHelpers.h"
#include "DataAssetFunctionLibrary.h"


/**
 *
 *
 *		GLOBAL STATS AND VALUES MANAGEMENTS
 *
 *
 */

/** Global Value management */
bool UKizuCombatComponent::IsEnoughValue(TEnumAsByte<EEffectorType> ValueType, float Value /*= 10*/)
{
	if (ValueType == EEffectorType::Health) {
		return IsEnoughHealth(Value);
	}
	else if (ValueType == EEffectorType::Mana) {
		return IsEnoughMana(Value);
	}
	else if (ValueType == EEffectorType::Energy) {
		return IsEnoughEnergy(Value);
	}
	return true;
}

/** HP Value management */
void UKizuCombatComponent::HealthChangeValue(float Value, TEnumAsByte<EValueChangesType> ChangesType) {
	if (ChangesType == EValueChangesType::Add) {
		CombatData.CurrentHP = UKizuGlobalFunctionLibrary::AddValue(Value, CombatData.MaxHP, CombatData.CurrentHP);
	}
	else if (ChangesType == EValueChangesType::Substract) {
		CombatData.CurrentHP = UKizuGlobalFunctionLibrary::SubstractValue(Value, CombatData.CurrentHP);
		CheckDeath();
	}
}
bool UKizuCombatComponent::IsEnoughHealth(float Value)
{
	return (CombatData.CurrentHP >= Value);
}

/** Mana Value management */
void UKizuCombatComponent::ManaChangeValue(float Value, TEnumAsByte<EValueChangesType> ChangesType) {
	if (ChangesType == EValueChangesType::Add) {
		CombatData.CurrentMana = UKizuGlobalFunctionLibrary::AddValue(Value, CombatData.MaxMana, CombatData.CurrentMana);
	}
	else if (ChangesType == EValueChangesType::Substract) {
		CombatData.CurrentMana = UKizuGlobalFunctionLibrary::SubstractValue(Value, CombatData.CurrentMana);
	}
}
bool UKizuCombatComponent::IsEnoughMana(float Value)
{
	return (CombatData.CurrentMana >= Value);
}

/** Energy Value management */
void UKizuCombatComponent::EnergyChangeValue(float Value, TEnumAsByte<EValueChangesType> ChangesType)
{
	if (ChangesType == EValueChangesType::Add) {
		CombatData.CurrentEnergy = UKizuGlobalFunctionLibrary::AddValue(Value, CombatData.MaxEnergy, CombatData.CurrentEnergy);
	}
	else if (ChangesType == EValueChangesType::Substract) {
		CombatData.CurrentEnergy = UKizuGlobalFunctionLibrary::SubstractValue(Value, CombatData.CurrentEnergy);
	}
}
bool UKizuCombatComponent::IsEnoughEnergy(float Value)
{
	return (CombatData.CurrentEnergy >= Value);
}

/** Regen Player Values over time */
void UKizuCombatComponent::TickRegen()
{
	RegenDelegate.BindUFunction(this, FName("RegenManager"));
	GetWorld()->GetTimerManager().SetTimer(RegenTimeHandle, RegenDelegate, FMath::RandRange(0.f, 0.1f) + 0.5, false);
}
void UKizuCombatComponent::RegenManager()
{
	if (CombatState != ECombatState::Dead)
	{
		HealthChangeValue(CombatData.RegenHP, EValueChangesType::Add);
		ManaChangeValue(CombatData.RegenMana, EValueChangesType::Add);
		EnergyChangeValue(CombatData.RegenEnergy, EValueChangesType::Add);
	}

	// Tick the Regen Again
	GetWorld()->GetTimerManager().ClearTimer(RegenTimeHandle);
	TickRegen();
}

/** Death */
void UKizuCombatComponent::Death()
{
	if (CombatState != ECombatState::Dead) {

		if (CombatAnimSetData.DeathAnimations.IsValidIndex(0)) {

			int32 RandomInteger = FMath::RandRange(0, CombatAnimSetData.DeathAnimations.Num() - 1);
			if (CombatAnimSetData.DeathAnimations[RandomInteger])
				UKizuCombatComponent::PlayAnimation(CombatAnimSetData.DeathAnimations[RandomInteger], 1.f);
		}
		CombatState = ECombatState::Dead;
	}
}
bool UKizuCombatComponent::CheckDeath(bool bToKill) {
	if (CombatData.CurrentHP <= 0)
	{
		if (bToKill) Death();
		return true;
	}
	return false;
}

/** Hit React */
void UKizuCombatComponent::HitReact(AActor* DamageCauser)
{
	if (DamageCauser) {
		ACharacter* CharacterDamageCauser = Cast <ACharacter>(DamageCauser);

		if (CombatState != ECombatState::Dead && CombatState != ECombatState::Attacked)
		{
			CombatState = ECombatState::Attacked;
			UAnimMontage* ReactMontage = UKizuCombatFunctionLibrary::GetMontageReact(CombatAnimSetData, UKizuCombatFunctionLibrary::GetDirection(CharRef, CharacterDamageCauser));
			if (ReactMontage) {
				PlayAnimation(ReactMontage);
			}
			HitReactEndDelegate.BindUFunction(this, FName("HitReactEnd"));
			GetWorld()->GetTimerManager().SetTimer(HitReactTimeHandler, HitReactEndDelegate, 0.5f, false);
		}
	}
}
void UKizuCombatComponent::HitReactEnd()
{
	if (CombatState == ECombatState::Attacked) {
		CombatState = ECombatState::Idle;
	}
	GetWorld()->GetTimerManager().ClearTimer(HitReactTimeHandler);
}

// Called on the First Game Frame
void UKizuCombatComponent::BeginPlay()
{
	Super::BeginPlay();


	TickRegen();
}

// Called every frame
void UKizuCombatComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (CharRef)
		if (CharRef->GetDistanceTo(TargetCharacter) > 2000)
			TargetCharacter = nullptr;
	// ...
}


/**
 *
 *
 *		WEAPONS
 *
 *
 */

/** GetWeapon from type */
AKizuWeapon * UKizuCombatComponent::GetWeapon(TEnumAsByte<EWeaponType> WeaponType, bool &Found)
{

		for (AKizuWeapon *Weapon : WeaponsArray)
		{
			if (Weapon->WeaponData.WeaponType == WeaponType)
			{
				Found = true;
				return Weapon;
			}
		}
		Found = false;
	return nullptr;
}

/** Weapon Socket Name */
FName UKizuCombatComponent::GetWeaponSocket(TEnumAsByte<EWeaponType> WeaponType, bool Sheath)
{
	FString SocketName = "";
	if (Sheath) {
		SocketName = "Sheath";
	}
	else if (!Sheath) {
		SocketName = "Unsheath";
	}

	if (WeaponType == EWeaponType::DefaultWeapon) SocketName.Append("Fist");
	else if (WeaponType == EWeaponType::Fist) SocketName.Append("Fist");
	else if (WeaponType == EWeaponType::Orb) SocketName.Append("Orb");
	else if (WeaponType == EWeaponType::Staff) SocketName.Append("Staff");
	else if (WeaponType == EWeaponType::Sword1H) SocketName.Append("S1H");
	else if (WeaponType == EWeaponType::Sword2H) SocketName.Append("S2H");
	else if (WeaponType == EWeaponType::SwordAndShield) SocketName.Append("SAS");
	SocketName = "None";

	return FName(*SocketName);
}

/** Call the Spell Casting */
void UKizuCombatComponent::CastSpell(FName SpellName)
{
	UKizuCombatFunctionLibrary::CastSpell(this, SpellName);
}

/** Play Ability */
void UKizuCombatComponent::PlayAbility(FName AbilityName)
{
	if (CombatState != ECombatState::Dead) {
		bool AbilityFound = false;
		FAbility AbilityToPlay = UDataAssetFunctionLibrary::GetAbility(CombatAnimSetData.Abilities, AbilityName, AbilityFound);
		if (AbilityFound) {
			//bool OnCooldown = false;
			//FAbilityCooldown AbilityCooldown = FindCooldown(AbilityName, OnCooldown);
			//if (OnCooldown) {
			//	//Inform the player that it is on CoolDown
			//}
			//else {
				//AbilityCooldown.CooldownEndDelegate.BindUFunction(this, FName("EndCooldown"));
				//GetWorld()->GetTimerManager().SetTimer(AbilityCooldown.CooldownTimeHandler, AbilityCooldown.CooldownEndDelegate, AbilityCooldown.Cooldown, false);
				////CooldownManager.Add(AbilityCooldown); This one is causing a crash
			UKizuCombatFunctionLibrary::PlayAbility(this, AbilityToPlay);
			//}
		}
	}
}
bool UKizuCombatComponent::PlayAbilityWithObjectiveCheck(FName AbilityName)
{
	//bool CanBePlayed = false;
	//FObjectives_Ability Objective_Ability =  UObjectivesFunctionLibrary::FindObjectivesByAbility(AbilityName, AbilityObjectives, CanBePlayed);
	//if (CanBePlayed) {
	//	//UKizuGlobalFunctionLibrary::KizuMessage((Objective_Ability.Objectives.bIsDone) ? "<Objectives::Debug> Ability can be played" : "<Objectives::Debug> Ability canNOT be played" );
	//	if (Objective_Ability.Objectives.bIsDone == true)
	//	{
	//		//UKizuGlobalFunctionLibrary::KizuMessage("<Objectives::Debug> All Objectives are done, playing ");
	//		PlayAbility(AbilityName);
	//		return true;
	//	}

	//}
		
	return false;
}



/** Cooldown Manager */
FAbilityCooldown UKizuCombatComponent::FindCooldown(FName AbilityID, bool &found) {
	found = false;
	FAbilityCooldown NullCD;

	for (FAbilityCooldown TempCooldown : CooldownManager)
	{
		if (TempCooldown.CurrentAbility.AbilityID == AbilityID) {
			found = true;
			return TempCooldown;
		}
	}
	return NullCD;
}
int  UKizuCombatComponent::GetCooldownIndex(FAbilityCooldown AbilityCooldown) {
	int i = -1;
	for (FAbilityCooldown TempCD : CooldownManager) {
		i++;
		if (AbilityCooldown.CurrentAbility.AbilityID == TempCD.CurrentAbility.AbilityID) {
			return i;
		}
	}
	return -1;
}
void UKizuCombatComponent::EndCooldown(FAbilityCooldown AbilityCooldown) {
	if (AbilityCooldown.CooldownTimeHandler.IsValid())
	{
		GetWorld()->GetTimerManager().ClearTimer(AbilityCooldown.CooldownTimeHandler);
		int CoolDownIndex = GetCooldownIndex(AbilityCooldown);
		if (CoolDownIndex != -1)
			CooldownManager.RemoveAt(CoolDownIndex);
	}
}

/** Play Combo */
void UKizuCombatComponent::PlayCombo()
{
	UPROPERTY()
	TArray<UAnimMontage*> MontagesToPlay = CombatAnimSetData.ComboMontages;
	UKizuCombatFunctionLibrary::PlayCombo(this, MontagesToPlay);
}
void UKizuCombatComponent::ResetCombo()
{
	
}


/** Targeting */
void UKizuCombatComponent::ToTarget()
{
	UKizuCombatFunctionLibrary::ToTarget(this);
}
void UKizuCombatComponent::ForgetTarget()
{
	TargetCharacter = nullptr;
}
ACharacter* UKizuCombatComponent::GetTarget()
{
	if (TargetCharacter)
		return TargetCharacter;
	else return nullptr;
}


/** Weapon Attach / Detach */
void UKizuCombatComponent::WeaponState(AKizuWeapon * Weapon, bool Sheath)
{
	if (Sheath) {
		Weapon->AttachToComponent(Mesh, FAttachmentTransformRules::KeepRelativeTransform, GetWeaponSocket(Weapon->WeaponData.WeaponType, true));
		WeaponOn = EWeaponType::DefaultWeapon;
	}
	else {
		Weapon->AttachToComponent(Mesh, FAttachmentTransformRules::KeepRelativeTransform, GetWeaponSocket(Weapon->WeaponData.WeaponType, false));
		WeaponOn = Weapon->WeaponData.WeaponType;
	}
}


/**
 * 
 *
 *		DAMAGE
 *
 *
 */
void UKizuCombatComponent::DamageOnce(AActor* Actor, float Value = 0.f)
{
	if (Actor) {
		if (!ActorsToHit.Contains(Actor)) {
			UGameplayStatics::ApplyDamage(Actor, Value, UKizuCombatComponent::GetOwner()->GetInstigatorController(), CharRef, nullptr);
			ActorsToHit.AddUnique(Actor);
		}
	}
}
void UKizuCombatComponent::EndDamageOnce()
{
	ActorsToHit.Empty();
}


// TRACE And DAMAGE
void UKizuCombatComponent::TraceAndDamageLoop(FName TraceStart = FName(TEXT("DamageStart")), FName TraceEnd = FName(TEXT("DamageEnd")), float Width = 100.0f, bool ShowDebugLine) {
	TArray<FHitResult> HitResult;
	FVector StartDamageLocation = Mesh->GetSocketLocation(TraceStart);
	FVector EndDamageLocation = Mesh->GetSocketLocation(TraceEnd);
	FCollisionShape ColSphere = FCollisionShape::MakeSphere(Width);
	FCollisionQueryParams CollisionParameters;
	CollisionParameters.AddIgnoredActor(CharRef);

	bool isHit = GetWorld()->SweepMultiByObjectType(HitResult, StartDamageLocation, EndDamageLocation, FQuat::Identity, FCollisionObjectQueryParams(ECC_TO_BITFIELD(ECC_WorldStatic) | ECC_TO_BITFIELD(ECC_WorldDynamic) | ECC_TO_BITFIELD(ECC_Pawn)), ColSphere, CollisionParameters);
	if (ShowDebugLine)
		DrawDebugLine(GetWorld(), StartDamageLocation, EndDamageLocation, FColor::Purple, true, 0.5f, (uint8)'\000', 1);

	for (auto& Hit : HitResult) {
		AActor* TempActor = Hit.GetActor();
		/*FString ActorName = TempActor->GetName();*/
		if (TempActor)
			DamageOnce(TempActor, 20);
	}
}
void UKizuCombatComponent::TraceAndDamageEnd()
{
	EndDamageOnce();
}


//	Dodge
void UKizuCombatComponent::DodgeByDirection(EDirection DodgingDirection)
{
	bool FoundDodge;
	UAnimMontage* AnimToPlay = UKizuCombatFunctionLibrary::FindDodgeFromAnimSet(FoundDodge, CombatAnimSetData, DodgingDirection);
	if (FoundDodge) PlayAnimation(AnimToPlay);
	else UKizuGlobalFunctionLibrary::KizuMessage("<Combat::Dodging> Dodge Not Found for [" + CharRef->GetName() + "]");
}
void UKizuCombatComponent::DodgeByCameraVelocity()
{
	EDirection DodgingDirection;
	UCameraComponent* CameraResult = CharRef->FindComponentByClass<UCameraComponent>();
	if (CameraResult->IsValidLowLevel() && CameraResult->IsActive()) {
		FRotator CameraRotation = CameraResult->GetComponentRotation();
		FVector ResultVector = CameraRotation.UnrotateVector(CharRef->GetVelocity());
		ResultVector.Normalize();
		if (ResultVector.X > 0.5)
			DodgingDirection = EDirection::FrontSide;
		else if (ResultVector.X < -0.5)
			DodgingDirection = EDirection::BackSide;
		else if (ResultVector.Y > 0.5)
			DodgingDirection = EDirection::RightSide;
		else if (ResultVector.Y < -0.5)
			DodgingDirection = EDirection::LeftSide;
		else DodgingDirection = EDirection::FrontSide;

		DodgeByDirection(DodgingDirection);
	}
}

