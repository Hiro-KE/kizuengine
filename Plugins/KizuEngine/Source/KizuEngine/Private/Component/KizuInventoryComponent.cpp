// Fill out your copyright notice in the Description page of Project Settings.
#include "KizuInventoryComponent.h"
#include "KizuGlobalFunctionLibrary.h"
#include "DrawDebugHelpers.h"
#include "KizuCharacter.h"
#include "KizuInventoryFunctionLibrary.h"


bool UKizuInventoryComponent::RemoveItem(FInventoryItem TempItem)
{
	int ItemIndex = GetIndex(TempItem);
	if (ItemIndex != -1) {
		Inventory.RemoveAt(ItemIndex);
		return true;
	}
	return false;
}

bool UKizuInventoryComponent::AddItem(FInventoryItem TempItem)
{
	if (Inventory.Num() < InventorySize) {
		Inventory.Add(TempItem);
		UKizuGlobalFunctionLibrary::KizuMessage("Item [" + TempItem.Name.ToString() +"] added to the inventory");
		return true;
	}
	else {
		UKizuGlobalFunctionLibrary::KizuMessage("Item [" + TempItem.Name.ToString() + "] could not be added to the inventory");
		return false;
	}
	return false;
}

int UKizuInventoryComponent::GetIndex(FInventoryItem Item)
{
	int Index = -1;
	for (FInventoryItem TempItem : Inventory) {
		Index++;
		if (Item.Name == TempItem.Name) {
			return Index;
		}
	}
	return -1;
}

bool UKizuInventoryComponent::CombineItems(TArray<FInventoryItem> ItemsToCombine, TArray<FInventoryItem> ItemsResult)
{
	for (FInventoryItem TempItem : ItemsToCombine)
	{
		if (GetIndex(TempItem) == -1)
			return false;
	}
	for (FInventoryItem TempItem : ItemsToCombine) {
		RemoveItem(TempItem);
	}
	for (FInventoryItem TempItem : ItemsResult) {
		AddItem(TempItem);
	}
	return true;
}

bool UKizuInventoryComponent::DropItem(FInventoryItem ItemToDrop)
{
	bool Result;
	if (ItemToDrop.IsValid())
	{
		Result = RemoveItem(ItemToDrop);
		if (Result) {
			UKizuInventoryFunctionLibrary::SpawnItem(CharRef, ItemToDrop);
			UKizuGlobalFunctionLibrary::KizuMessage("Item [" + ItemToDrop.Name.ToString() + "] dropped");
			return true;
		}
	}
	UKizuGlobalFunctionLibrary::KizuMessage("Item [" + ItemToDrop.Name.ToString() + "] could not be dropped");
	return false;
}

bool UKizuInventoryComponent::UseItem(FInventoryItem ItemToUse)
{
	if (ItemToUse.ItemType == EItemType::Consummable) {
		if (AKizuCharacter* KizuCharRef = Cast<AKizuCharacter>(CharRef)) {
			if (UKizuInventoryFunctionLibrary::ConsumeItem(ItemToUse, KizuCharRef->GetCombatComponent()))
				RemoveItem(ItemToUse);
		}
	}
	else if(ItemToUse.ItemType == EItemType::Weapon) {
		
	}
	else if (ItemToUse.ItemType == EItemType::Armor) {

	}
	else UKizuGlobalFunctionLibrary::KizuMessage("Invalid Item Type or Item can't be used.");
	return false;
}

bool UKizuInventoryComponent::PickUpItem(AKizuItem* ItemToPick)
{
	FInventoryItem ItemData = ItemToPick->GetData();
	if (ItemData.IsValid()) {
		AddItem(ItemData);
		ItemToPick->Destroy(true);
		UKizuGlobalFunctionLibrary::KizuMessage("Item [" + ItemData.Name.ToString() + "] has been picked");
		return true;
	}
	else {
		UKizuGlobalFunctionLibrary::KizuMessage("Item [" + ItemData.Name.ToString() + "] could not be picked");
		return true;
	}
}

UPROPERTY()
FCollisionShape InteractionSphere = FCollisionShape::MakeSphere(100.f);
UPROPERTY()
TArray<FHitResult> InteractionSphereHits;
UPROPERTY()
FVector TempCharLocation;
AKizuItem* UKizuInventoryComponent::FindCloseItem()
{
	TempCharLocation = CharRef->GetActorLocation();

	//DrawDebugSphere(GetWorld(), TempCharLocation, InteractionSphere.GetSphereRadius(), 50, FColor::Purple, true);
	
	bool isHit = GetWorld()->SweepMultiByChannel(InteractionSphereHits, TempCharLocation, TempCharLocation, FQuat::Identity, ECC_GameTraceChannel6, InteractionSphere); //ECC_GameTraceChannel6 is for Items
	if (isHit)
	{
		// loop through TArray
		for (auto& Hit : InteractionSphereHits)
		{
			if (Hit.GetComponent()->GetCollisionObjectType() == ECC_GameTraceChannel6)
			{
				if (AKizuItem* ItemFound = Cast<AKizuItem>(Hit.GetActor()))
					return ItemFound;
			}
		}
	}
	return nullptr;
}

bool UKizuInventoryComponent::FindAndPickItem()
{
	AKizuItem* FoundItem = FindCloseItem();
	if (FoundItem) {
		PickUpItem(FoundItem);
		return true;
	}
	else return false;
}
