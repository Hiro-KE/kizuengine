// Fill out your copyright notice in the Description page of Project Settings.

#include "KizuMainComponent.h"
#include "Animation/AnimInstance.h"
#include "FunctionLibrary/ObjectivesFunctionLibrary.h"
#include "KizuGameState.h"
#include "KizuGlobalFunctionLibrary.h"

// Sets default values for this component's properties
UKizuMainComponent::UKizuMainComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bReplicates = true;
	// ...



}


// Called when the game starts
void UKizuMainComponent::BeginPlay()
{
	Super::BeginPlay();
	bReplicates = true;
	SetReferences();
	
	
}

void UKizuMainComponent::SetReferences()
{
	AActor *TempOwner = GetOwner();
	PawnRef = Cast <APawn>(TempOwner);

	if (PawnRef->IsValidLowLevel()) {
		CharRef = Cast <ACharacter>(PawnRef);
	}
	if (CharRef->IsValidLowLevel()) {
		Mesh = CharRef->GetMesh();
	}
	if (Mesh->IsValidLowLevel()) {
		AnimInstance = Mesh->GetAnimInstance();
	}
	if (GetWorld()->GetGameState()) {
		AKizuGameState* GameState = Cast <AKizuGameState>(GetWorld()->GetGameState());
		if (GameState)
		{
			if (GameState->SpellDataAsset)
				SpellData = GameState->SpellDataAsset->SpellList;
		}
	}
}


// Called every frame
void UKizuMainComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	// ...
}


// Animation
void UKizuMainComponent::PlayAnimation(UAnimMontage * Animation, float PlayRate)
{
	if (Animation->IsValidLowLevel())
		AnimInstance->Montage_Play(Animation, PlayRate, EMontagePlayReturnType::Duration, 0.0f, true);
}

void UKizuMainComponent::InitializeObjectives()
{
	
	//for (FObjectives_Ability AbilityObjective : AbilityObjectives) {
	//	AbilityObjective.Objectives.OwnerMainComponent = this;
	//	UKizuGlobalFunctionLibrary::KizuMessage("TESTING CURRENTLY?");
	//}
}

void UKizuMainComponent::FinishObjectiveForAbility(FName ObjectiveName)
{
	//UObjectivesFunctionLibrary::FinishObjectiveByComp(ObjectiveName, this);
}
//
//void UKizuMainComponent::OnObjectiveUpdate_Native(const FObjectives& Objective)
//{
//	
//
//
//	OnObjectiveUpdate(Objective);
//}

