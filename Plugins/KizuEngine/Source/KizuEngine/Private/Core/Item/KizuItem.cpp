// Fill out your copyright notice in the Description page of Project Settings.

#include "KizuItem.h"
#include "DrawDebugHelpers.h"

// Sets default values
AKizuItem::AKizuItem()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;


	//Components
	/*MESH*/
	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
	SetRootComponent(Mesh);

	/*SPHERE COMPONENT*/
	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	CollisionSphere->SetSphereRadius(100);
	CollisionSphere->AttachToComponent(Mesh, FAttachmentTransformRules::KeepRelativeTransform);
	CollisionSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionSphere->SetGenerateOverlapEvents(true);
	CollisionSphere->SetCollisionObjectType(ECC_GameTraceChannel6); //ECC_GameTraceChannel6 is for Items
}

// Called when the game starts or when spawned
void AKizuItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AKizuItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//DrawDebugSphere(GetWorld(), CollisionSphere->GetComponentLocation(), 100, 10, FColor(181, 22, 250), true, 0.0, 0, 1);

}

void AKizuItem::OnConstruction(const FTransform &Transform)
{

	
}

void AKizuItem::Initialize(FInventoryItem Data)
{
	this->ItemData = Data;
	this->Mesh->SetSkeletalMesh(ItemData.Mesh);
}

