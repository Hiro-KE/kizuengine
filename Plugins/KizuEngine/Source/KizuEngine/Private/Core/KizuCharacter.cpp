// Fill out your copyright notice in the Description page of Project Settings.

#include "KizuCharacter.h"
#include "Engine/World.h"
#include "KizuCombatComponent.h"
#include "KizuGlobalFunctionLibrary.h"
#include "KizuTypes.h"
#include "KizuAIController.h"
#include "HUD/KizuWidget.h"
#include "Blueprint/UserWidget.h"
#include "Components/WidgetComponent.h"
#include "Core/Objectives/KizuObjective.h"
#include "KizuInventoryComponent.h"



// Sets default values
AKizuCharacter::AKizuCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Inventory
	InventoryComponent = CreateDefaultSubobject<UKizuInventoryComponent>("InventoryComponent");
	InventoryComponent->bAutoActivate = false;
	InventoryComponent->bIsActive = false;
	
	//Combat
	CombatComponent = CreateDefaultSubobject<UKizuCombatComponent>("CombatComponent");
	CombatComponent->bAutoActivate = false;
	CombatComponent->bIsActive = false;

	//Sensing
	PawnSensing = CreateDefaultSubobject<UPawnSensingComponent>("PawnSensing");
	PawnSensing->OnSeePawn.AddDynamic(this, &AKizuCharacter::OnSeePawn);
	PawnSensing->OnHearNoise.AddDynamic(this, &AKizuCharacter::OnHearNoise);
	PawnSensing->SensingInterval = 0.5f;
	PawnSensing->SetPeripheralVisionAngle(85.f);
	PawnSensing->SightRadius = 500.f;
	PawnSensing->bOnlySensePlayers = false;
	bCanSeePawn = true;
	
	//Ai Controller
	this->AIControllerClass = AKizuAIController::StaticClass();
	this->AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	//Objective
	for (TSubclassOf<UKizuObjective> ObjectiveClass : ObjectiveClassList) {
		UKizuObjective* Objective = NewObject<UKizuObjective>(this, ObjectiveClass);
		Objective->SetOwner(this);
		ObjectiveList.Add(Objective);
	}

}


// Called when the game starts or when spawned
void AKizuCharacter::BeginPlay()
{
	Super::BeginPlay();

	/** Initialize Character's component **/
	InitiateCharacter();
}



// Called every frame
void AKizuCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}




// Called to bind functionality to input
void AKizuCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

float AKizuCharacter::TakeDamage(float Damage, FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{
	CombatComponent->HealthChangeValue(Damage, EValueChangesType::Substract);
	
	//Play Reaction
	if (Damage != 0) {
		CombatComponent->HitReact(DamageCauser);
	}

	//Play Death
	if (CombatComponent->CombatData.CurrentHP <= 0.0f)
		CombatComponent->Death();

	return 0.0f;
}


/*INITIALIZE START*/


//Checker
template<typename Base, typename T>
inline bool instanceof(const T *ptr) {
	return dynamic_cast<const Base*>(ptr) != nullptr;
}

void AKizuCharacter::InitializeAiData()
{
	this->AIControllerClass = AKizuAIController::StaticClass();

	if (CharacterDataAsset->AiDataAsset) {
		PawnSensing->SensingInterval = CharacterDataAsset->AiDataAsset->SensingInterval;
		PawnSensing->SetPeripheralVisionAngle(CharacterDataAsset->AiDataAsset->VisionAngleDegree);
		PawnSensing->SightRadius = CharacterDataAsset->AiDataAsset->SightRadius;
		PawnSensing->bHearNoises = true;
		PawnSensing->HearingThreshold = CharacterDataAsset->AiDataAsset->HearingDistance;
		PawnSensing->LOSHearingThreshold = CharacterDataAsset->AiDataAsset->HearingDistance;
		UKizuGlobalFunctionLibrary::KizuMessage("Ai Data Added to " + GetName());
		if (instanceof<AKizuAIController>(GetController()))
		{
			Cast<AKizuAIController>(GetController())->SetAiData(CharacterDataAsset->AiDataAsset);
		}
		else {
			
			//CurrentController->SetAiData(CharacterDataAsset->AiDataAsset);
		}

	}
}

void AKizuCharacter::InitializeInventoryData()
{
	if (CharacterDataAsset->InventoryComponent) {
		InventoryComponent->bIsActive = true;
		UKizuGlobalFunctionLibrary::KizuMessage("Inventory Component Added to " + GetName());

		//Inventory Data
		/**
		 * Placing here the inventory Data
		 */
		InventoryComponent->InventorySize = CharacterDataAsset->InventorySize;
	}
}

void AKizuCharacter::InitializeCombatData()
{
	if (CharacterDataAsset->CombatComponent) {

		//Activating the CombatComp
		CombatComponent->bIsActive = true;
		UKizuGlobalFunctionLibrary::KizuMessage("Combat Component Added to " + GetName());

		//Combat 
		if (CharacterDataAsset->CombatDataAsset->IsValidLowLevel())
		{

			//Combat Stats
			CombatComponent->CombatData = CharacterDataAsset->CombatDataAsset->CombatData;
			UKizuGlobalFunctionLibrary::KizuMessage("Combat Stats Added to the Combat Component for " + GetName());

			//Combat AnimSet DATA
			if (CharacterDataAsset->CombatDataAsset->CombatAnimSet_DataAsset)
			{
				CombatComponent->CombatAnimSetData = CharacterDataAsset->CombatDataAsset->CombatAnimSet_DataAsset->CombatAnimSet;
				UKizuGlobalFunctionLibrary::KizuMessage("Combat AnimSet Added to the Combat Component for " + GetName());
			}
			else UKizuGlobalFunctionLibrary::KizuMessage("Combat AnimSet not found for " + GetName());

			//Ability Objectives
			/*if (CharacterDataAsset->CombatDataAsset->IsValidLowLevel())
			{
				CombatComponent->AbilityObjectives = CharacterDataAsset->CombatDataAsset->AbilityObjective_DataAsset->AbilityObjectives;
				UKizuGlobalFunctionLibrary::KizuMessage("Ability Objectives Added to the Combat Component for " + GetName());
			}
			else UKizuGlobalFunctionLibrary::KizuMessage("Ability Objectives not found for " + GetName());*/
		}
		else UKizuGlobalFunctionLibrary::KizuMessage("Combat Data unavailable for " + GetName() + ". Default ones will be added.");


	}
}


void AKizuCharacter::InitiateCharacter()
{
	if (CharacterDataAsset->IsValidLowLevel()) {
		InitializeCombatData();
		InitializeInventoryData();
		InitializeAiData();

		OnFinishInitialization_Native();
	}
	else UKizuGlobalFunctionLibrary::KizuMessage("No Character Data Asset Found for " + GetName());
}

void AKizuCharacter::OnFinishInitialization_Native()
{
	
	if (CombatComponent->IsActive()) {
		//Initialize Objective by Setting up the reference of this component in each objective 
		CombatComponent->InitializeObjectives();
	}

	// BP purpose
	OnFinishInitialization();
}

/*INITIALIZE END*/



void AKizuCharacter::InitializeWidget(UWidgetComponent* WidgetComponent, TSubclassOf<UKizuWidget> WidgetClass, AKizuCharacter * Character)
{
	UPROPERTY()
		UKizuWidget *WidgetInstance = CreateWidget<UKizuWidget>(GetWorld(), WidgetClass);
	WidgetInstance->SetCharacter(Character);
	WidgetComponent->SetWidget(WidgetInstance);
}

void AKizuCharacter::InitializeWidget_ThisCharacter(UWidgetComponent* WidgetComponent, TSubclassOf<UKizuWidget> WidgetClass)
{
	InitializeWidget(WidgetComponent, WidgetClass, this);
}


void AKizuCharacter::OnSeePawn(APawn *OtherPawn)
{
	if (OtherPawn && bCanSeePawn) {
		PawnSensed = OtherPawn;
		bCanSeePawn = false;
		DelayEnableSeePawn();
	}
}



void AKizuCharacter::OnHearNoise(APawn *OtherActor, const FVector &Location, float Volume)
{

}


void AKizuCharacter::DelayEnableSeePawn()
{
	UPROPERTY()
	FTimerHandle TimeHandler;
	if (UWorld* CurrentWorld = AKizuCharacter::GetWorld())
		if (CharacterDataAsset->AiDataAsset) { 
			CurrentWorld->GetTimerManager().SetTimer(TimeHandler, [this]() {PawnSensed = nullptr; bCanSeePawn = true; }, CharacterDataAsset->AiDataAsset->PawnSensingMemoryInterval, false); }
		else { 
			CurrentWorld->GetTimerManager().SetTimer(TimeHandler, [this]() {PawnSensed = nullptr; bCanSeePawn = true; }, 5, false); }
	
}

void AKizuCharacter::SetTaskAsDone(FName Name)
{
	for (UKizuObjective* CurrentObjective : ObjectiveList) {
		CurrentObjective->SetTaskDone(Name);
	}
}

void AKizuCharacter::OnObjectiveDone_Native(UKizuObjective* &Objective)
{


	// Use for Blueprint Implementable Event
	OnObjectiveDone(Objective);
}
