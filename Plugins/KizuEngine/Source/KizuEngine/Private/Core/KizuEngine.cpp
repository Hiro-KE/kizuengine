// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "KizuEngine.h"
#include "KizuEngineEdMode.h"

#define LOCTEXT_NAMESPACE "FKizuEngineModule"

void FKizuEngineModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	FEditorModeRegistry::Get().RegisterMode<FKizuEngineEdMode>(FKizuEngineEdMode::EM_KizuEngineEdModeId, LOCTEXT("KizuEngineEdModeName", "KizuEngineEdMode"), FSlateIcon(), true);
}

void FKizuEngineModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	FEditorModeRegistry::Get().UnregisterMode(FKizuEngineEdMode::EM_KizuEngineEdModeId);
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FKizuEngineModule, KizuEngine)