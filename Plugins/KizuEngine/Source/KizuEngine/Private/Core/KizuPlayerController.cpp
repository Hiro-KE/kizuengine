// Fill out your copyright notice in the Description page of Project Settings.


#include "KizuPlayerController.h"
#include "FunctionLibrary/KizuGlobalFunctionLibrary.h"
#include "Runtime/LevelSequence/Public/LevelSequence.h"
#include "Runtime/LevelSequence/Public/LevelSequencePlayer.h"
#include "KizuCharacter.h"

void AKizuPlayerController::KizuPossess(APawn* NewPawn)
{
	//Check if the Pawn is valid
	if (NewPawn) {
		//Check if the character of this controller is a KizuChar
		if (AKizuCharacter* OldCharacter = Cast<AKizuCharacter>(GetCharacter())) {

			//Inform the logs of the process
			UKizuGlobalFunctionLibrary::KizuMessage("<PlayerController> [" + GetName() +
				"] Moving from [" + OldCharacter->GetName() +
				"] To [" + NewPawn->GetName() + "]");

			//GetReady the Target controller for the switching
			AController* TargetController = NewPawn->GetController();
			
			//Switching Controllers
			Possess(NewPawn);
			if (TargetController)
				TargetController->Possess(OldCharacter);

			//Initializing Data
			OldCharacter->InitializeAiData();

			//Call when done possessing
			OnEndKizuPosses(true);
		}
		else {
			Possess(NewPawn);
			//Call when done possessing
			OnEndKizuPosses(false);
		}
	}
	//Call when done possessing (could not perform the KizuPosses)
	else OnEndKizuPosses(false);
}

void AKizuPlayerController::PlaySequence(ULevelSequence* SequencetoPlay)
{
	ALevelSequenceActor* LevelSequenceActor;

	// Setup the sequence player
	if (SequencetoPlay && LevelSequencePlayer == nullptr)
		LevelSequencePlayer = ULevelSequencePlayer::CreateLevelSequencePlayer(GetWorld(), SequencetoPlay, FMovieSceneSequencePlaybackSettings(), LevelSequenceActor);

	//Sequence Play
	if (LevelSequencePlayer)
	{
		LevelSequencePlayer->Play();
	}
}

