// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/KizuSpell.h"
#include "FunctionLibrary/KizuCombatFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "FunctionLibrary/KizuGlobalFunctionLibrary.h"
#include "Components/PrimitiveComponent.h"

// Sets default values
AKizuSpell::AKizuSpell()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	//Capsule Component
		SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Capsule Component"));
		RootComponent = SphereComponent;
	//Projectile Movement
		ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement Component"));


	
}

// Called when the game starts or when spawned
void AKizuSpell::BeginPlay()
{
	Super::BeginPlay();


	//Life Span
	SetLifeSpan(SpellData.LifeSpan);
}

// Called every frame
void AKizuSpell::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}


void AKizuSpell::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != GetOwner() && OwnerCombatComponent)
	{
		if (UKizuCombatFunctionLibrary::CheckFaction(GetOwner(), OtherActor) == SpellData.FactionToAffect) {
			UKizuCombatComponent* OtherActorCombatComp = UKizuCombatFunctionLibrary::GetCombat(OtherActor);
			for (FSpellEffect SpellEffect : SpellData.SpellEffects)
			{
				UKizuCombatFunctionLibrary::ApplySpellEffects(OwnerCombatComponent, OtherActorCombatComp, SpellEffect);
			}
			EndSpell();
		}
	}
}

void AKizuSpell::StartSpell()
{

	// Projectile Collision
	if (SphereComponent) {
		SphereComponent->SetSphereRadius(SpellData.SpellRadius, true);
		SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &AKizuSpell::OnOverlapBegin);
		SphereComponent->SetGenerateOverlapEvents(true);
		SphereComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	}


	// Projectile Movement
	if (ProjectileMovement) {
		ProjectileMovement->InitialSpeed = 5000.f;
		ProjectileMovement->MaxSpeed = 5000.f;
		ProjectileMovement->bRotationFollowsVelocity = true;
		ProjectileMovement->bShouldBounce = false;
		ProjectileMovement->ProjectileGravityScale = 0.f;


		if (OwnerCombatComponent) {
			ProjectileMovement->Velocity = OwnerCombatComponent->CharRef->GetActorForwardVector() * SpellData.ProjectileSpeed;


			//Set Spell at the casting socket location
			if (OwnerCombatComponent->Mesh)
				this->SetActorLocation(OwnerCombatComponent->Mesh->GetSocketLocation(SpellData.CastingSocket));

				

			if (SpellData.SpellType == ESpellType::Projectile) {

			}

			else if (SpellData.SpellType == ESpellType::Instant) {
				if (OwnerCombatComponent->TargetCharacter) {
					ProjectileMovement->Velocity = FVector::ZeroVector;
					USkeletalMeshComponent* TargetMesh = OwnerCombatComponent->TargetCharacter->GetMesh();
					if (TargetMesh) {
					//	this->AttachToComponent(TargetMesh, FAttachmentTransformRules::KeepRelativeTransform, SpellData.TargetSocket);
						SetActorLocation(OwnerCombatComponent->TargetCharacter->GetActorLocation());
					}
				}
			}

			else if (SpellData.SpellType == ESpellType::HomingProjectile) {
				if (OwnerCombatComponent->TargetCharacter) {
					ProjectileMovement->HomingTargetComponent = OwnerCombatComponent->TargetCharacter->GetRootComponent();
					ProjectileMovement->HomingAccelerationMagnitude = SpellData.ProjectileSpeed * 10;
					ProjectileMovement->bIsHomingProjectile = true;
				}
			}

			else if (SpellData.SpellType == ESpellType::BouncingProjectile) {
				ProjectileMovement->bShouldBounce = true;
				SphereComponent->SetEnableGravity(true);
				ProjectileMovement->ProjectileGravityScale = 1.f;
			}
		}

	}

	
	// Projectile effects
	if (OwnerCombatComponent) {
		if (this) {
			//EMITTERS
			ParticleSystem_TempStart = UGameplayStatics::SpawnEmitterAttached(SpellData.ParticleSystem_Start, OwnerCombatComponent->Mesh, SpellData.CastingSocket);
			ParticleSystem_TempLoop = UGameplayStatics::SpawnEmitterAttached(SpellData.ParticleSystem_Loop, RootComponent);

			//SOUNDS
			Sound_TempStart = UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SpellData.Sound_Start, GetActorLocation());
			Sound_TempLoop = UGameplayStatics::SpawnSoundAttached(SpellData.Sound_Loop, RootComponent, NAME_None);
		}
	}

	//LifeSpan
	SetLifeSpan(SpellData.LifeSpan);

}




void AKizuSpell::EndSpell()
{
	//Emitters
		ParticleSystem_TempEnd = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), SpellData.ParticleSystem_End, GetTransform(), true);
		if (ParticleSystem_TempLoop)
			ParticleSystem_TempLoop->DestroyComponent();

	//Sounds
		Sound_TempEnd = UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SpellData.Sound_End, GetActorLocation());
		if (Sound_TempLoop)
			Sound_TempLoop->DestroyComponent();

	//ActorFinalize
		if (this)
			this->K2_DestroyActor();
	
}

void AKizuSpell::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	EndSpell();
}

