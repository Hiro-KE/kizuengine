// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/Objectives/KizuObjective.h"
#include "Engine/Engine.h"
#include "GameFramework/Character.h"
#include "Runtime/LevelSequence/Public/LevelSequence.h"
#include "Runtime/LevelSequence/Public/LevelSequencePlayer.h"
#include "..\..\..\Public\Core\Objectives\KizuObjective.h"


bool UKizuObjective::SetTaskDone(FName TaskName)
{
	{
		if (ObjectiveDetails.SetTaskDoneByName(TaskName)) {

			OnTaskUpdate_Native(TaskName);

			if (ObjectiveDetails.ObjectiveUpdate()) {
				OnObjectiveDone_Native();
			}

			return true;
		}
	}
	return false;
}


void UKizuObjective::OnTaskUpdate_Native(FName& TaskName)
{
	


	// Blueprint Event Implementation
	OnTaskUpdate(TaskName);
}

void UKizuObjective::OnObjectiveDone_Native()
{


	// Blueprint Event Implementation
	OnObjectiveDone();
}
