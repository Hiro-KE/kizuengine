// Fill out your copyright notice in the Description page of Project Settings.

#include "DataAssetFunctionLibrary.h"
#include "WeaponDataAsset.h"
#include "SpellDataAsset.h"
#include "ItemDataAsset.h"


FWeaponData UDataAssetFunctionLibrary::GetWeaponFromDataAsset(UDataAsset * DataAsset, FName WeaponID, bool& Found)
{
	UPROPERTY()
	UWeaponDataAsset * WeaponDataAsset;
	WeaponDataAsset = Cast<UWeaponDataAsset>(DataAsset);
	if (WeaponDataAsset)
	{
		for (FWeaponData WeaponTemp : WeaponDataAsset->Weapons)
		{
			if (WeaponTemp.ID == WeaponID)
			{
				Found = true;
				return WeaponTemp;
			}
		}
	}
	Found = false;
	FWeaponData EmptyData;
	return EmptyData;
}

FSpell UDataAssetFunctionLibrary::GetSpell(UDataAsset * DataAsset, FName SpellID, bool & Found)
{
	UPROPERTY()
		 USpellDataAsset* SpellDataAseet;
	SpellDataAseet = Cast<USpellDataAsset>(DataAsset);
	if (SpellDataAseet)
	{
		for (FSpell Spell : SpellDataAseet->SpellList)
		{
			if (Spell.Name == SpellID)
			{
				Found = true;
				return Spell;
			}
		}
	}
	Found = false;
	FSpell EmptyData;
	return EmptyData;
}

FSpell UDataAssetFunctionLibrary::GetSpell(TArray<FSpell> SpellList, FName SpellID, bool & Found)
{
	for (FSpell Spell : SpellList)
	{
		if (Spell.Name == SpellID)
		{
			Found = true;
			return Spell;
		}
	}

	Found = false;
	FSpell EmptyData;
	return EmptyData;
}

FAbility UDataAssetFunctionLibrary::GetAbility(TArray<FAbility> AbilityList, FName AbilityID, bool& Found)
{
	for (FAbility AbilityTemp : AbilityList)
	{
		if (AbilityTemp.AbilityID == AbilityID) {
			Found = true;
			return AbilityTemp;
		}
	}

	Found = false;
	FAbility EmptyData;
	return EmptyData;
}

FInventoryItem UDataAssetFunctionLibrary::GetItem(TArray<FInventoryItem> ItemsList, FName ItemName, bool& Found)
{
	for (FInventoryItem ItemTemp : ItemsList) {
		if (ItemTemp.Name == ItemName) {
			Found = true;
			return ItemTemp;
		}
	}

	Found = false;
	FInventoryItem EmptyData;
	return EmptyData;
}

FInventoryItem UDataAssetFunctionLibrary::GetItem(UDataAsset* DataAsset, FName ItemName, bool& Found)
{
	UItemDataAsset* ItemDataAsset = Cast<UItemDataAsset>(DataAsset);
	return GetItem(ItemDataAsset->InventoryItems, ItemName, Found);
}

