// Fill out your copyright notice in the Description page of Project Settings.

#include "KizuCombatFunctionLibrary.h"
#include "KizuGlobalFunctionLibrary.h"
#include "KizuSpell.h"
#include "DataAssetFunctionLibrary.h"


/** Get Combat from an Actor **/
 UKizuCombatComponent*  UKizuCombatFunctionLibrary::GetCombat(AActor* TargetActor)
	{
	 if (TargetActor)
	 {
		 UKizuCombatComponent *CombatComponent = Cast <UKizuCombatComponent>(TargetActor->GetComponentByClass(UKizuCombatComponent::StaticClass()));
		 if (CombatComponent) return CombatComponent; 
	 }
	return nullptr;
}

 

 //Combos
 void UKizuCombatFunctionLibrary::PlayCombo(UKizuCombatComponent * CombatComponent, TArray<UAnimMontage*> MontagesToPlay)
 {
	 UPROPERTY()
	 int32 TotalNumber = MontagesToPlay.Num();

	 UPROPERTY()
	 FString TotalNumberString;
	 TotalNumberString.AppendInt(CombatComponent->CurrentComboNumber);

	 if (UKizuCombatFunctionLibrary::CheckState(CombatComponent, ECombatState::Idle)) {

		 
		 if (CombatComponent->CurrentComboNumber < TotalNumber) {
			 UPROPERTY()
				 UAnimMontage *ComboToPlay = MontagesToPlay[CombatComponent->CurrentComboNumber];
				if (ComboToPlay) {
					 CombatComponent->PlayAnimation(ComboToPlay, 1.0f);
					 CombatComponent->CombatState = ECombatState::Attacking;
				}
				else CombatComponent->CurrentComboNumber = 0;

				 if (CombatComponent->CurrentComboNumber >= TotalNumber - 1)
					 CombatComponent->CurrentComboNumber = 0;
				 else
					 CombatComponent->CurrentComboNumber++;
		 }


		 else {
			 CombatComponent->CurrentComboNumber = 0;
		 }
	 }
 }
 void UKizuCombatFunctionLibrary::ComboCounter(UKizuCombatComponent * CombatComponent, TEnumAsByte<EComboState> ComboState)
 {
	 if (ComboState == EComboState::NextCombo)
	 {
		 if (UKizuCombatFunctionLibrary::CheckState(CombatComponent, ECombatState::Attacking))
			 CombatComponent->CombatState = ECombatState::Idle;
	 }
		 
	 else {
		 CombatComponent->CurrentComboNumber = 0;
		 CombatComponent->CombatState = ECombatState::Idle;
	 }
 }


 //Spells 
 AKizuSpell* UKizuCombatFunctionLibrary::SpawnSpell(AActor* Spawner)
 {
	 UWorld* World =  Spawner->GetWorld();
	 UPROPERTY()
		 AKizuSpell* ActorToSpawn;
	 if (World)
	 {
		 FActorSpawnParameters SpawnParams;
		 SpawnParams.Owner = Spawner;
		 FRotator Rotation = Spawner->GetActorRotation();
		 FVector Location = Spawner->GetActorLocation();
		 ActorToSpawn = World->SpawnActor<AKizuSpell>(Location, Rotation, SpawnParams);
		 return ActorToSpawn;
	 }
	 return nullptr;
 }
 void UKizuCombatFunctionLibrary::CastSpell(UKizuCombatComponent* CombatComponent, FName SpellName)
 {
	 if (CombatComponent) {
		 bool Found;
		 FSpell SpellData = UDataAssetFunctionLibrary::GetSpell(CombatComponent->SpellData, SpellName, Found);
		 if (Found) {
			 bool IsEnoughResources = Consumption(CombatComponent, SpellData);
			 if (IsEnoughResources) {
				 AKizuSpell* SpellToSpawn;



				 if (SpellData.SpellType == ESpellType::Custom) {
					 UClass* ClassToSpawn = SpellData.CustomSpell->StaticClass();
					 //ClassToSpawn CustomSpellSpawned;
						 //= UKizuGlobalFunctionLibrary::SpawnActor(CombatComponent->CharRef, ClassToSpawn);
					 //UKizuGlobalFunctionLibrary::KizuMessage(CustomSpellSpawned->GetName());
				 }
				 else {
					 SpellToSpawn = UKizuCombatFunctionLibrary::SpawnSpell(CombatComponent->CharRef);
					 if (SpellToSpawn) {
						 SpellToSpawn->SpellData = SpellData;
						 SpellToSpawn->SetOwner(CombatComponent->CharRef);
						 SpellToSpawn->SetOwnerCombatComponent(CombatComponent);

						 SpellToSpawn->StartSpell();
					 }
				 }
			 }
		 }
	 }
 }
 bool UKizuCombatFunctionLibrary::Consumption(UKizuCombatComponent* CombatComponent, FSpell SpellData)
 {
	 for (FSpellEffect SpellEffect : SpellData.SpellConsumption) {

			 // Is Adding a value
			 if (SpellEffect.ValueChangesType == EValueChangesType::Add) {
				 ApplySpellEffects(CombatComponent, CombatComponent, SpellEffect);
			 }

			 // Is Subtracting a value ( Checks needed ) 
			 else if (SpellEffect.ValueChangesType == EValueChangesType::Substract) {
				 if (CombatComponent->IsEnoughValue(SpellEffect.Effect, SpellEffect.Amount))
					 ApplySpellEffects(CombatComponent, CombatComponent, SpellEffect);
				 else return false;
			 }
		 
	 }
	return true;
 }
 void UKizuCombatFunctionLibrary::ApplySpellEffects(UKizuCombatComponent* SelfCombatComponent, UKizuCombatComponent* TargetCombatComponent, FSpellEffect SpellEffect)
 {
	 if (SpellEffect.Effect == EEffectorType::Health) {
		 if (SpellEffect.ValueChangesType == EValueChangesType::Add) {
			 TargetCombatComponent->HealthChangeValue(SpellEffect.Amount, EValueChangesType::Add);
		 }
		 else if (SpellEffect.ValueChangesType == EValueChangesType::Substract)
		 {
			 SelfCombatComponent->DamageOnce(TargetCombatComponent->CharRef, SpellEffect.Amount);
			 SelfCombatComponent->EndDamageOnce();
		 } 
	 }
	 if (SpellEffect.Effect == EEffectorType::Mana) {
		 TargetCombatComponent->ManaChangeValue(SpellEffect.Amount, SpellEffect.ValueChangesType);
	 }
	 if (SpellEffect.Effect == EEffectorType::Energy) {
		 TargetCombatComponent->EnergyChangeValue(SpellEffect.Amount, SpellEffect.ValueChangesType);
	 }
 }

 //Ability 
 void UKizuCombatFunctionLibrary::PlayAbility(UKizuCombatComponent* CombatComponent, FAbility AbilityToPlay)
 {
	 if (AbilityToPlay.MontageToPlay) {
		 CombatComponent->PlayAnimation(AbilityToPlay.MontageToPlay);
	 } 
 }

 //Dodging
 UAnimMontage* UKizuCombatFunctionLibrary::FindDodgeFromAnimSet(bool& Found, FCombatAnimSet CombatAnimSet, EDirection DodgingDirection /*= EDirection::FrontSide*/)
 {
	 for (FDodgeData Dodge : CombatAnimSet.Dodges) {
		 if (Dodge.DodgingDirection == DodgingDirection) {
			 Found = true;
			 return Dodge.MontageToPlay;
		 }
	 }
	 Found = false;
	 return nullptr;
 }

 //HitReact
 TEnumAsByte<EDirection> UKizuCombatFunctionLibrary::GetDirection(ACharacter* CharacterA, ACharacter* CharacterB)
 {
	 if (CharacterA && CharacterB) {
		 FVector NormCharOrientA = UKizuGlobalFunctionLibrary::GetNormalizedOrientation(CharacterA->GetMesh());
		 FVector NormCharOrientB = UKizuGlobalFunctionLibrary::GetNormalizedOrientation(CharacterB->GetMesh());
		 float OrientationDotProduct = FVector::DotProduct(NormCharOrientA, NormCharOrientB);
		 if (OrientationDotProduct >= -1.f || OrientationDotProduct <= -0.7f) return EDirection::FrontSide;
		 else if (OrientationDotProduct >= 0.7f || OrientationDotProduct <= 1.f) return EDirection::BackSide;
		 else if (OrientationDotProduct >= 0.f || OrientationDotProduct <= 0.7f) return EDirection::RightSide;
		 else if (OrientationDotProduct >= -0.7f || OrientationDotProduct <= 0.f) return EDirection::LeftSide;
	 }
	
	 return EDirection::FrontSide;

 }
 UAnimMontage* UKizuCombatFunctionLibrary::GetMontageReact(FCombatAnimSet CombatAnimSet, TEnumAsByte<EDirection> Direction)
 {
	 for (FHitReact HitReactData : CombatAnimSet.HitReacts)
	 {
		 if (HitReactData.Direction == Direction) return HitReactData.Animation;
	 }
	 return nullptr;
 }


 //CombatState
 bool UKizuCombatFunctionLibrary::CheckState(UKizuCombatComponent* CombatComponent, TEnumAsByte<ECombatState> CombatState)
 {
	 return CombatComponent->CombatState == CombatState;
 }


 //Returns True if same faction, Returns false if not same faction
 TEnumAsByte<EFaction> UKizuCombatFunctionLibrary::CheckFaction(AActor* ActorA, AActor* ActorB)
 {
	 if (ActorA && ActorB) {
		 UKizuCombatComponent* CombatComponentA = UKizuCombatFunctionLibrary::GetCombat(ActorA);
		 UKizuCombatComponent* CombatComponentB = UKizuCombatFunctionLibrary::GetCombat(ActorB);
		 if (CombatComponentA && CombatComponentB) {
			 if (CombatComponentA->CombatData.FactionNumber == CombatComponentB->CombatData.FactionNumber) {
				 return EFaction::Ally;
			 }
			 else return EFaction::Enemy;
		 }
	 }
	 return EFaction::Neutral;
 }


 //Returns True if Target is valid, Returns False if there is no target
 bool UKizuCombatFunctionLibrary::ToTarget(UKizuCombatComponent* CombatComponent, float TargetRange)
 {
		 ACharacter* Character = CombatComponent->CharRef;
		 FVector StartTrace = Character->GetActorLocation();
		 FVector EndTrace = Character->GetActorLocation() + (Character->GetActorForwardVector() * TargetRange);
		 TArray<AActor*> ToIgnore;
		 ToIgnore.Add(Character);

		 TArray<FHitResult> HitResult = UKizuGlobalFunctionLibrary::MultiTrace(Character, StartTrace, EndTrace, 1000, ToIgnore, true);

		 if (!CombatComponent->TargetCharacter) {
			 if (HitResult.IsValidIndex(0))
				 for (FHitResult Hit : HitResult) {
					 AActor* HitTarget = Hit.GetActor();
					 if (UKizuCombatFunctionLibrary::GetCombat(HitTarget)) {
						 CombatComponent->TargetCharacter = Cast <ACharacter> (HitTarget);
					 return true;
					 }
				}
			else return false;
		 }
		 else {
			 bool SameTargetFound = false;
			 if (HitResult.IsValidIndex(0)) {
				 for (FHitResult Hit : HitResult) {
					 AActor* HitTarget = Hit.GetActor();

					 //Found same Actor in zone
					 if (HitTarget == CombatComponent->TargetCharacter) {
						 SameTargetFound = true;
					 }
					 //Setting Another Target In Zone
					 else if (UKizuCombatFunctionLibrary::GetCombat(HitTarget)) {
						 CombatComponent->TargetCharacter = Cast <ACharacter> (HitTarget);
						 return true;
					 }
				 }
				 if (SameTargetFound)
					 CombatComponent->ForgetTarget();
				 else return false;
			 }
			 else return false;
		 }
		 return false;
 }

