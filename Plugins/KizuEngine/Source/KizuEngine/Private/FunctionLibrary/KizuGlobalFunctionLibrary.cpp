// Fill out your copyright notice in the Description page of Project Settings.

#include "KizuGlobalFunctionLibrary.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetSystemLibrary.h"


float UKizuGlobalFunctionLibrary::AddValue(float Value, float MaxValue, float CurrentValue)
{
	if ((CurrentValue + Value) < MaxValue) {
		return (Value + CurrentValue);
	}
	else return MaxValue;
}

float UKizuGlobalFunctionLibrary::SubstractValue(float Value, float CurrentValue)
{
	if((CurrentValue - Value) <= 0)
		return 0.0f;
	else return (CurrentValue - Value) ;
}

void UKizuGlobalFunctionLibrary::KizuMessage(FString StringToDisplay)
{
	StringToDisplay = "**** KIZU MESSAGE ::  " + StringToDisplay + " :: END KIZU MESSAGE ****";

	UE_LOG(LogTemp, Warning, TEXT("%s"), *StringToDisplay);
}

TArray<FHitResult> UKizuGlobalFunctionLibrary::MultiTrace(AActor* TracingActor, FVector StartTrace, FVector EndTrace, float Width, TArray<AActor*> ToIgnore, bool DrawDebug)
{
	TArray<FHitResult> HitResult;
	FCollisionShape ColSphere = FCollisionShape::MakeSphere(Width);
	FCollisionQueryParams CollisionParameters;

	for (AActor* ActorToIgnore : ToIgnore)
	{CollisionParameters.AddIgnoredActor(ActorToIgnore);}

	bool isHit = TracingActor->GetWorld()->SweepMultiByObjectType(HitResult, StartTrace, EndTrace, FQuat::Identity, FCollisionObjectQueryParams(ECC_TO_BITFIELD(ECC_WorldStatic) | ECC_TO_BITFIELD(ECC_WorldDynamic) | ECC_TO_BITFIELD(ECC_Pawn)), ColSphere, CollisionParameters);
	
	/*if (DrawDebug)
	{
		EDrawDebugTrace::Type DebugType;
		DebugType= EDrawDebugTrace::Type::ForDuration;
		DrawDebugSphereTraceMulti(TracingActor->GetWorld(), StartTrace, EndTrace, Width, DebugType, isHit, HitResult, FColor::Red, FColor::Blue, 0.5f);
	}*/
	return HitResult;

}

void UKizuGlobalFunctionLibrary::DebugSphere(AActor* DebuggingActor, FVector SphereLocation, float Width)
{
	DrawDebugSphere(DebuggingActor->GetWorld(), SphereLocation, Width, 50, FColor::Red, true, 1.f);
}

FVector UKizuGlobalFunctionLibrary::GetNormalizedOrientation(USceneComponent* SceneComponent)
{
	if (SceneComponent) {
		FVector ComponentLocation = SceneComponent->GetComponentLocation();
		FVector ComponentForward = SceneComponent->GetForwardVector();
		FVector TempOrientation = ComponentLocation - (ComponentLocation + ComponentForward);
		return TempOrientation.GetSafeNormal();
	}
	else return FVector::ZeroVector;
}

