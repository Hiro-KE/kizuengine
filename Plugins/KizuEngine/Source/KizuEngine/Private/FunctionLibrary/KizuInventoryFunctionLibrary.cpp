// Fill out your copyright notice in the Description page of Project Settings.

#include "KizuInventoryFunctionLibrary.h"
#include "KizuGlobalFunctionLibrary.h"



UKizuInventoryComponent* UKizuInventoryFunctionLibrary::GetInventoryComponent(AActor* TargetActor)
{
	if (TargetActor)
		{
		UKizuInventoryComponent *InventoryComponent = Cast <UKizuInventoryComponent>(TargetActor->GetComponentByClass(UKizuInventoryComponent::StaticClass()));
			if (InventoryComponent) return InventoryComponent;
		}
	return nullptr;
}

TArray<FInventoryItem> UKizuInventoryFunctionLibrary::GetInventory(AActor* TargetActor, bool& Found)
{
	if (UKizuInventoryComponent* TargetInventoryComponent = GetInventoryComponent(TargetActor)) {
		Found = true;
		return TargetInventoryComponent->GetInventory();
	}
	Found = false;
	TArray<FInventoryItem> TempNull;
	return TempNull;
}

AKizuItem* UKizuInventoryFunctionLibrary::SpawnItem(AActor* Spawner, FInventoryItem ItemData)
{
	UWorld* World = Spawner->GetWorld();
	UPROPERTY()
		AKizuItem* ActorToSpawn;
	if (World)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = Spawner;
		FRotator Rotation = Spawner->GetActorRotation();
		FVector Location = Spawner->GetActorLocation();
		ActorToSpawn = World->SpawnActor<AKizuItem>(Location, Rotation, SpawnParams);
		ActorToSpawn->Initialize(ItemData);
		return ActorToSpawn;
	}
	return nullptr;
}

bool UKizuInventoryFunctionLibrary::ConsumeItem(FInventoryItem ItemToConsume, UKizuCombatComponent* CombatComponent)
{
	if(CombatComponent)
	if(CombatComponent->IsActive()){
		for (auto& EffectorData : ItemToConsume.ConsumableData.Effectors) {
			if (EffectorData.Effector == EEffectorType::Health) {
				CombatComponent->HealthChangeValue(EffectorData.Amount, EffectorData.ValueChangesType);
			}
			if (EffectorData.Effector == EEffectorType::Mana) {
				CombatComponent->ManaChangeValue(EffectorData.Amount, EffectorData.ValueChangesType);
			}
			if (EffectorData.Effector == EEffectorType::Energy) {
				CombatComponent->EnergyChangeValue(EffectorData.Amount, EffectorData.ValueChangesType);
			}
		}
		return true;
	}
		
	else {
		UKizuGlobalFunctionLibrary::KizuMessage("[" + CombatComponent->CharRef->GetName() + "]" + " does not have combat enabled");
		return false;
	}
	return false;
}

bool UKizuInventoryFunctionLibrary::EquipWeapon(FInventoryItem ItemToEquip, UKizuCombatComponent* CombatComponent)
{
	return false;
}

bool UKizuInventoryFunctionLibrary::EquipArmor(FInventoryItem ItemToEquip, UKizuCombatComponent* CombatComponent)
{
	return false;
}

void UKizuInventoryFunctionLibrary::DestroyItem(AKizuItem* ItemToDestroy)
{
	if (ItemToDestroy)
		ItemToDestroy->Destroy();
}

