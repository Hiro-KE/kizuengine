// Fill out your copyright notice in the Description page of Project Settings.


#include "ObjectivesFunctionLibrary.h"
#include "KizuGlobalFunctionLibrary.h"
#include "Component/KizuMainComponent.h"
#include "Runtime/LevelSequence/Public/LevelSequence.h"
//
//TArray<FObjectives_Ability> UObjectivesFunctionLibrary::FinishObjectiveByArray(FName ObjectiveName, TArray<FObjectives_Ability>	 &AbilityObjectivesToFetch)
//{
//	TArray<FObjectiveBase> ObjectivesToChange = GetObjectives(ObjectiveName, AbilityObjectivesToFetch);
//	for (auto &CurrentAbilityObjective : AbilityObjectivesToFetch)
//	{
//		for (auto &CurrentObjective : CurrentAbilityObjective.Objectives.Objectives) {
//			for (auto &ObjectiveToChange : ObjectivesToChange) {
//				//UKizuGlobalFunctionLibrary::KizuMessage("<Objectives::Debug> Checking " + ObjectiveToChange.ObjectiveName.ToString());
//				if (CurrentObjective.IsEqual(ObjectiveToChange)) {
//					//UKizuGlobalFunctionLibrary::KizuMessage("<Objectives::Debug> Finishing Objective " + ObjectiveName.ToString());
//					CurrentAbilityObjective.Objectives = SetObjectiveDoneByName(ObjectiveName, CurrentAbilityObjective.Objectives);
//				}
//			}
//		}
//	}
//	return AbilityObjectivesToFetch;
//}
//
//TArray<FObjectives_Ability> UObjectivesFunctionLibrary::FinishObjectiveByComp(FName ObjectiveName, UKizuMainComponent* MainComponent)
//{
//	for (FObjectives_Ability &AbilityObjective : MainComponent->AbilityObjectives) {
//		AbilityObjective.Objectives.OwnerMainComponent = MainComponent;
//	}
//	MainComponent->AbilityObjectives = FinishObjectiveByArray(ObjectiveName, MainComponent->AbilityObjectives);
//	return MainComponent->AbilityObjectives;
//}
//
//FObjectiveBase UObjectivesFunctionLibrary::GetSingleObjective(FName ObjectiveName, TArray<FObjectives_Ability> &AbilityObjectivesToFetch)
//{
//	bool Found = false;
//	FObjectiveBase ObjectiveFound;
//	for (FObjectives_Ability &CurrentAbilityObjectives : AbilityObjectivesToFetch) {
//		ObjectiveFound = CurrentAbilityObjectives.GetObjectiveBase(ObjectiveName, Found);
//		if (Found) return ObjectiveFound;
//	}
//	return ObjectiveFound;
//}
//
//TArray<FObjectiveBase> UObjectivesFunctionLibrary::GetObjectives(FName ObjectiveName, TArray<FObjectives_Ability> &AbilityObjectivesToFetch)
//{
//	bool Found = false;
//	TArray<FObjectiveBase> ObjectivesFound;
//	FObjectiveBase ObjectiveBaseTemp;
//	for (FObjectives_Ability &CurrentAbilityObjectives : AbilityObjectivesToFetch) {
//		//UKizuGlobalFunctionLibrary::KizuMessage("<Objectives::Debug> Looking for Objectives name : " + ObjectiveName.ToString());
//		ObjectiveBaseTemp = CurrentAbilityObjectives.GetObjectiveBase(ObjectiveName, Found);
//		if (Found) ObjectivesFound.Add(ObjectiveBaseTemp);
//	}
//	return ObjectivesFound;
//}
//
//FObjectives_Ability UObjectivesFunctionLibrary::FindObjectivesByAbility(FName AbilityName, TArray<FObjectives_Ability> &AbilityObjectivesToFetch, bool &Found)
//{
//	Found = false;
//	for (FObjectives_Ability &CurrentAbilityObjectives : AbilityObjectivesToFetch) {
//		for (FName &CurrentAbility : CurrentAbilityObjectives.Rewarded_Abilities) {
//			//UKizuGlobalFunctionLibrary::KizuMessage("<Objectives::Debug> Comparing Abilities " + CurrentAbility.ToString() + " to the search ability name : " + AbilityName.ToString());
//			if (CurrentAbility.IsEqual(AbilityName)) {
//				UKizuGlobalFunctionLibrary::KizuMessage("<Objectives::Debug> "+ AbilityName.ToString() +" Was Unlocked successfully ");
//				Found = true;
//				return CurrentAbilityObjectives;
//			}
//		}
//	}
//	return FObjectives_Ability();
//}
//
//FObjectives UObjectivesFunctionLibrary::SetObjectiveDoneByName(FName ObjectiveName, FObjectives &Objective) {
//	FObjectiveBase TempObjective;
//	bool IsFullyDone = true;
//	for (FObjectiveBase &CurrentObjective : Objective.Objectives) {
//		if (CurrentObjective.ObjectiveName == ObjectiveName) {
//			UKizuGlobalFunctionLibrary::KizuMessage("<Objectives::Debug> Setting Objective as done, name : " + ObjectiveName.ToString());
//			CurrentObjective.bIsDone = true;
//			TempObjective = CurrentObjective;
//		}
//		if (CurrentObjective.bIsDone == false) {
//			UKizuGlobalFunctionLibrary::KizuMessage("<Objectives::Debug> Objective having False value : " + CurrentObjective.ObjectiveName.ToString());
//			IsFullyDone = false;
//		}
//	}
//	
//	//Updating
//	UKizuGlobalFunctionLibrary::KizuMessage(IsFullyDone ? "<Objectives::Debug> Updating the global isDone to : True" : "<Objectives::Debug> Updating the global isDone to : False");
//	Objective.bIsDone = IsFullyDone;
//
//	UKizuGlobalFunctionLibrary::KizuMessage(Objective.OwnerMainComponent ? "The component has been correctly initialized" : "ERROR HAS OCCURED");
//	if (Objective.OwnerMainComponent) {
//		Objective.OwnerMainComponent->OnObjectiveUpdate_Native(Objective);
//	}
//
//	return Objective;
//}
//
//void UObjectivesFunctionLibrary::UpdateObjectiveStatus(UKizuMainComponent* MainComponent, TArray<FObjectives_Ability>& NewAbilityObjectives)
//{
//	MainComponent->AbilityObjectives = NewAbilityObjectives;
//}
//
//
