// Copyright (C) KizuTeam Ltd. 2019. All Rights Reserved.


#include "KizuWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/UserWidget.h"
#include "HUD/KizuItemWidget.h"
#include "Components/UniformGridSlot.h"
#include "FunctionLibrary/ObjectivesFunctionLibrary.h"

void UKizuWidget::InitializeReferences()
{
	if (GetWorld()) {
		if (!OtherCharacter)
			CharacterRef = Cast<AKizuCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		else CharacterRef = Cast<AKizuCharacter>(OtherCharacter);
		bPropertiesSet = true;
		OnFinishedInitialization();
	}
}

void UKizuWidget::SetCharacter(AKizuCharacter * NewCharacter)
{
	OtherCharacter = NewCharacter;
	InitializeReferences();
}


UKizuWidget::UKizuWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
	bPropertiesSet = false;
}

void UKizuWidget::NativeConstruct()
{
	// Do some custom setup

	// Call the Blueprint "Event Construct" node
	Super::NativeConstruct();
	
	//Initialize the player and his components
	InitializeReferences();

}

void UKizuWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTime)
{
	// Make sure to call the base class's NativeTick function
	Super::NativeTick(MyGeometry, InDeltaTime);

	// Do your custom tick stuff here
}



void UKizuWidget::FillUniformGridFromInventory(UUniformGridPanel* GridPanel, TSubclassOf<UKizuItemWidget> ItemWidgetClass, int ColumnCount)
{
	UPROPERTY()
		int CurrentColumn = 0;
	UPROPERTY()
		int CurrentRow = 0;
	

	GridPanel->ClearChildren();
	for (FInventoryItem InventoryItem : GetInventory()) {
		

		//Creating and initializing the Item Widget Data
		UPROPERTY()
		UKizuItemWidget *ItemWidgetInstance = CreateWidget<UKizuItemWidget>(GetWorld(), ItemWidgetClass);
		ItemWidgetInstance->SetWidgetContainer(this);
		ItemWidgetInstance->SetItemData(InventoryItem);

		//Filling the Uniform Grid and setting up the Alignments
		UUniformGridSlot* ItemSlot = GridPanel->AddChildToUniformGrid(ItemWidgetInstance);
		ItemSlot->SetHorizontalAlignment(HAlign_Fill);
		ItemSlot->SetVerticalAlignment(VAlign_Fill);
		if (CurrentColumn < ColumnCount - 1) {
			ItemSlot->SetColumn(CurrentColumn);
			ItemSlot->SetRow(CurrentRow);
			CurrentColumn++;
		}
		else {
			ItemSlot->SetColumn(CurrentColumn);
			ItemSlot->SetRow(CurrentRow);
			CurrentRow++;
			CurrentColumn = 0;
		}
	}
}

