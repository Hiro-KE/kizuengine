// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "KizuEngineEdMode.h"
#include "KizuEngineEdModeToolkit.h"
#include "Toolkits/ToolkitManager.h"
#include "EditorModeManager.h"

const FEditorModeID FKizuEngineEdMode::EM_KizuEngineEdModeId = TEXT("EM_KizuEngineEdMode");

FKizuEngineEdMode::FKizuEngineEdMode()
{

}

FKizuEngineEdMode::~FKizuEngineEdMode()
{

}

void FKizuEngineEdMode::Enter()
{
	FEdMode::Enter();

	if (!Toolkit.IsValid() && UsesToolkits())
	{
		Toolkit = MakeShareable(new FKizuEngineEdModeToolkit);
		Toolkit->Init(Owner->GetToolkitHost());
	}
}

void FKizuEngineEdMode::Exit()
{
	if (Toolkit.IsValid())
	{
		FToolkitManager::Get().CloseToolkit(Toolkit.ToSharedRef());
		Toolkit.Reset();
	}

	// Call base Exit method to ensure proper cleanup
	FEdMode::Exit();
}

bool FKizuEngineEdMode::UsesToolkits() const
{
	return true;
}




