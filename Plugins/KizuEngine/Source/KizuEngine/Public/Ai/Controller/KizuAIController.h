// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "AiDataAsset.h"
#include "KizuAIController.generated.h"


/**
 * 
 */
UCLASS()
class KIZUENGINE_API AKizuAIController : public AAIController
{
	GENERATED_BODY()
	
private:

	AKizuAIController();

protected:
	// Called when the game starts or when spawned
	UFUNCTION()
	virtual void BeginPlay() override;

public:


	UPROPERTY()
		uint8 bAiDataHasBeenSet:1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UAISenseConfig_Sight* SightConfig;

	UFUNCTION()
		void SetAiData(UAiDataAsset* AiData);

	/**
	 * Default Use (No Data available)
	 */
	void SetAiData();

	virtual void SetPawn(APawn* InPawn) override;



	//UFUNCTION(BlueprintCallable)
	//	void PerceptionSenseUpdate(const TArray<AActor*> &ActorsSensed);

	
};
