// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Ai/Service/KizuBTService.h"
#include "BTService_RoamingManager.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UBTService_RoamingManager : public UKizuBTService
{
	GENERATED_BODY()

public:

	/** Actor that the Ai will roam around it (Can be self or a specific object) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Defaults", meta = (ExposeOnSpawn = "true", AllowPrivateAccess = "true"))
		FBlackboardKeySelector ActorToRoamAround;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Defaults", meta = (ExposeOnSpawn = "true", AllowPrivateAccess = "true"))
		FBlackboardKeySelector NewLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Defaults", meta = (ExposeOnSpawn = "true", AllowPrivateAccess = "true"))
		float RoamingRadius = 500.f;

private:

	UPROPERTY()
		AActor* CurrentActor;
	UPROPERTY()
		FVector TempVector = FVector::ZeroVector;

	void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
