 //Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Ai/Service/KizuBTService.h"
#include "BTService_SensedActors.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UBTService_SensedActors : public UKizuBTService
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Defaults", meta = (ExposeOnSpawn = "true", AllowPrivateAccess = "true"))
		FBlackboardKeySelector SensedActorsKey;

	UPROPERTY()
		APawn* SensedPawn;


	
	void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

};
