// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Ai/Service/KizuBTService.h"
#include "Core/KizuTypes.h"
#include "BTService_SensedActors_Faction.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UBTService_SensedActors_Faction : public UKizuBTService
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Defaults", meta = (ExposeOnSpawn = "true", AllowPrivateAccess = "true"))
		FBlackboardKeySelector SensedActorsKey;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Defaults", meta = (ExposeOnSpawn = "true", AllowPrivateAccess = "true"))
		TEnumAsByte<EFaction> FactionToSense;

	UPROPERTY()
		APawn* SensedPawn;



	void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
