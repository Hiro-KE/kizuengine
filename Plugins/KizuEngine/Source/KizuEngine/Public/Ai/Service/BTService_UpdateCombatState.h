// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Ai/Service/KizuBTService.h"
#include "BTService_UpdateCombatState.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UBTService_UpdateCombatState : public UKizuBTService
{
	GENERATED_BODY()

		
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Defaults", meta = (ExposeOnSpawn = "true", AllowPrivateAccess = "true"))
		FBlackboardKeySelector CombatState;

		void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
