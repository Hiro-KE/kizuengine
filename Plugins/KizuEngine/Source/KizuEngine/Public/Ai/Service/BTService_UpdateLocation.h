// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Ai/Service/KizuBTService.h"
#include "BTService_UpdateLocation.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UBTService_UpdateLocation : public UKizuBTService
{
	GENERATED_BODY()

		UPROPERTY()
		AActor* TempActor;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Defaults", meta = (ExposeOnSpawn = "true", AllowPrivateAccess = "true"))
		FBlackboardKeySelector TargetActor;
		
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Defaults", meta = (ExposeOnSpawn = "true", AllowPrivateAccess = "true"))
		FBlackboardKeySelector LocationVector;

		void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
