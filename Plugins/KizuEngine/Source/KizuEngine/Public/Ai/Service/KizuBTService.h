// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "KizuCharacter.h"
#include "KizuAIController.h"
#include "KizuCombatFunctionLibrary.h"
#include "KizuInventoryFunctionLibrary.h"
#include "KizuBTService.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class KIZUENGINE_API UKizuBTService : public UBTService
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadOnly)
		AKizuCharacter* CharacterRef;
	UPROPERTY(BlueprintReadOnly)
		AKizuAIController* AiControllerRef;
	UPROPERTY(BlueprintReadOnly)
		UKizuCombatComponent* CombatComponent;
	UPROPERTY(BlueprintReadOnly)
		UKizuInventoryComponent* InventoryComponent;


	UPROPERTY()
		uint8 bDataHasBeenSet : 1;


	//Constructor
	UKizuBTService();

	/*Tick only on instance*/
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

	/** called when auxiliary node becomes active
	 * this function should be considered as const (don't modify state of object) if node is not instanced! */
	virtual void OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	UFUNCTION(BlueprintCallable)
		void SetReferences(UBehaviorTreeComponent* OwnerComp);

	UFUNCTION(BlueprintImplementableEvent)
		void OnTick(AKizuAIController* AiController, float DeltaSeconds);


};
