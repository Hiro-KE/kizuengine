// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Ai/Task/KizuBTTask.h"
#include "BTTask_UseAbility.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UBTTask_UseAbility : public UKizuBTTask
{
	GENERATED_BODY()
public :
	
	UPROPERTY(Category = Node, EditAnywhere)
		FName AbilityName;


	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
