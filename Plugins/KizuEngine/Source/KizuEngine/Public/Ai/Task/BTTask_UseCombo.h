// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Ai/Task/KizuBTTask.h"
#include "BTTask_UseCombo.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UBTTask_UseCombo : public UKizuBTTask
{
	GENERATED_BODY()


	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
