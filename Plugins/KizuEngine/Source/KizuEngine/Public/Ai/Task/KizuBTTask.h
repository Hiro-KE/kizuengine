// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "KizuCharacter.h"
#include "KizuAIController.h"
#include "KizuCombatFunctionLibrary.h"
#include "KizuInventoryFunctionLibrary.h"
#include "KizuBTTask.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class KIZUENGINE_API UKizuBTTask : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly)
		AKizuCharacter* CharacterRef;
	UPROPERTY(BlueprintReadOnly)
		AKizuAIController* AiControllerRef;
	UPROPERTY(BlueprintReadOnly)
		UKizuCombatComponent* CombatComponent;
	UPROPERTY(BlueprintReadOnly)
		UKizuInventoryComponent* InventoryComponent;

	UPROPERTY()
	uint8 bDataHasBeenSet:1;


	//Constructor
	UKizuBTTask();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	UFUNCTION(BlueprintCallable)
		void SetReferences(UBehaviorTreeComponent* OwnerComp);

	UFUNCTION(BlueprintImplementableEvent)
		void OnExecute(AKizuAIController* AiController);

	//virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
