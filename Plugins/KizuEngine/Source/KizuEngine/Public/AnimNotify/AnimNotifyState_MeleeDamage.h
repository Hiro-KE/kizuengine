// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotifyState.h"
#include "KizuCombatComponent.h"
#include "AnimNotifyState_MeleeDamage.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UAnimNotifyState_MeleeDamage : public UAnimNotifyState
{
	GENERATED_BODY()


public:

	//Constructor
	UAnimNotifyState_MeleeDamage();

	//Variables to edit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimNotify", meta = (ExposeOnSpawn = true))
		FName TraceStart = FName(TEXT("DamageStart"));
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimNotify", meta = (ExposeOnSpawn = true))
		FName TraceEnd = FName(TEXT("DamageEnd"));;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimNotify", meta = (ExposeOnSpawn = true))
		float Width = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimNotify", meta = (ExposeOnSpawn = true))
		uint8 DebugLine : 1;



	UPROPERTY()
		UKizuCombatComponent* CombatComp;

	virtual void NotifyBegin(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation, float TotalDuration) override;
	virtual void NotifyTick(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation, float FrameDeltaTime) override;
	virtual void NotifyEnd(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation) override;
	
	
};
