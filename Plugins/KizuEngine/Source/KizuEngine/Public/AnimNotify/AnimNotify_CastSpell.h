// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "KizuTypes.h"
#include "KizuCombatComponent.h"
#include "AnimNotify_CastSpell.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UAnimNotify_CastSpell : public UAnimNotify
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimNotify", meta = (ExposeOnSpawn = true))
		FName SpellName;

	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
};
