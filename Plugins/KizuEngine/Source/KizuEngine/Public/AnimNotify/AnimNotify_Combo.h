// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "KizuTypes.h"
#include "KizuCombatComponent.h"
#include "AnimNotify_Combo.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UAnimNotify_Combo : public UAnimNotify
{
	GENERATED_BODY()
public:

		virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimNotify", meta = (ExposeOnSpawn = true))
			TEnumAsByte<EComboState> ComboState;


};


