// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "KizuMainComponent.h"
#include "KizuCombatComponent.generated.h"


USTRUCT(BlueprintType)
struct FAbilityCooldown {
	GENERATED_USTRUCT_BODY();
	float Cooldown;
	FAbility CurrentAbility;
	FTimerHandle CooldownTimeHandler;
	FTimerDelegate CooldownEndDelegate;
};

/**
 *
 */
UCLASS()
class KIZUENGINE_API UKizuCombatComponent : public UKizuMainComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(VisibleAnywhere, SaveGame)
		TArray<AKizuWeapon*> WeaponsArray;
	UPROPERTY(VisibleAnywhere, SaveGame)
		ACharacter* TargetCharacter;
	
	/* Handle to manage the HitReact timer */
	FTimerHandle HitReactTimeHandler;
	FTimerDelegate HitReactEndDelegate;

	/* Handle to manage the RegenTick */
	FTimerHandle RegenTimeHandle;
	FTimerDelegate RegenDelegate;

	//CooldownManager
	TArray<FAbilityCooldown> CooldownManager;
	


	//LocalTempVars
	UPROPERTY()
		TArray<AActor*> ActorsToHit;
	UPROPERTY()
		TEnumAsByte<EWeaponType> WeaponOn;
	UPROPERTY()
		int32 CurrentComboNumber = 0;
	UPROPERTY(BlueprintReadWrite, SaveGame)
		TEnumAsByte<ECombatState> CombatState;


	/**
	 * Getter
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure)
		TEnumAsByte<ECombatState> GetCombatState() { return CombatState; }

	// Called on the First Game Frame
	virtual void BeginPlay() override;
	// Called Every Frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	/**
	 * 
	 *
	 *	Value changers
	 *
	 */


	 /** Global Value management */
	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsEnoughValue(TEnumAsByte<EEffectorType> ValueType, float Value = 10);

	 /** Health Value Changes */
	UFUNCTION(BlueprintCallable)
		void HealthChangeValue(float Value, TEnumAsByte<EValueChangesType> ChangesType);
	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsEnoughHealth(float Value);

	/** Mana Value Changes */
	UFUNCTION(BlueprintCallable)
		void ManaChangeValue(float Value, TEnumAsByte<EValueChangesType> ChangesType);
	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsEnoughMana(float Value);

	/** Energy Value Changes */
	UFUNCTION(BlueprintCallable)
		void EnergyChangeValue(float Value, TEnumAsByte<EValueChangesType> ChangesType);
	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsEnoughEnergy(float Value);

	/** Regen Player Values over time */
	UFUNCTION(BlueprintCallable)
		void TickRegen();
	UFUNCTION(BlueprintCallable)
		void RegenManager();

	//Death
	UFUNCTION(BlueprintCallable)
		void Death();
	UFUNCTION(BlueprintCallable)
		bool CheckDeath(bool bToKill = true);

	//Hit React
	UFUNCTION(BlueprintCallable)
		void HitReact(AActor* DamageCauser);
	UFUNCTION(BlueprintCallable)
		void HitReactEnd();


	//Damage
	UFUNCTION(BlueprintCallable)
		void DamageOnce(AActor* Actor, float Value);
	UFUNCTION(BlueprintCallable)
		void EndDamageOnce();

	/**
 *
 *
 *		WEAPONS
 *
 *
 */
	UFUNCTION(BlueprintCallable)
		void WeaponState(AKizuWeapon *Weapon, bool Sheath);
	UFUNCTION(BlueprintCallable)
		AKizuWeapon* GetWeapon(TEnumAsByte<EWeaponType> WeaponType, bool &Found);
	UFUNCTION(BlueprintCallable)
		FName GetWeaponSocket(TEnumAsByte<EWeaponType> WeaponType, bool Sheath);
	



	/**
	 * Spells
	 */
	UFUNCTION(BlueprintCallable)
		void CastSpell(FName SpellName);

	/**
	 * Ability
	 */
	UFUNCTION(BlueprintCallable)
		void PlayAbility(FName AbilityName);
	UFUNCTION(BlueprintCallable)
		bool PlayAbilityWithObjectiveCheck(FName AbilityName);

	/**
	 * Combos
	 */
	UFUNCTION(BlueprintCallable)
		void PlayCombo();
	UFUNCTION(BlueprintCallable)
		void ResetCombo();



	/**
	 * TARGET
	 */
	UFUNCTION(BlueprintCallable)
		void ToTarget();
	UFUNCTION(BlueprintCallable)
		void ForgetTarget();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		ACharacter* GetTarget();

	/**
	 *	TRACING
	 */
	UFUNCTION(BlueprintCallable)
		void TraceAndDamageLoop(FName TraceStart, FName TraceEnd, float Width, bool ShowDebugLine = false);
	UFUNCTION(BlueprintCallable)
		void TraceAndDamageEnd();

	/**
	 * DODGING
	 */
	UFUNCTION(BlueprintCallable)
		void DodgeByDirection(EDirection DodgingDirection);
	UFUNCTION(BlueprintCallable)
		void DodgeByCameraVelocity();
	/**
	 * COOLDOWN MANAGER
	 */
	UFUNCTION(BlueprintCallable)
		FAbilityCooldown FindCooldown(FName AbilityID, bool &found);
	UFUNCTION(BlueprintCallable)
		int GetCooldownIndex(FAbilityCooldown AbilityCooldown);
	UFUNCTION(BlueprintCallable)
		void EndCooldown(FAbilityCooldown AbilityCooldown);


};

//// Mana value change (Server)
//UFUNCTION(Reliable, Server, WithValidation, BlueprintCallable)
//	void Server_ManaChangeValue(float Value, bool Force);
//	void Server_ManaChangeValue_Implementation(float Value, bool Force);
//	bool Server_ManaChangeValue_Validate(float Value, bool Force);
//// Mana value change (Client)
//	UFUNCTION(Unreliable, Client, BlueprintCallable)
//		void Client_ManaChangeValue(float Value, bool Force);


// Mana value change (MultiCast)