// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Component/KizuMainComponent.h"
#include "Core/KizuTypes.h"
#include "Core/Item/KizuItem.h"
#include "KizuInventoryComponent.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UKizuInventoryComponent : public UKizuMainComponent
{
	GENERATED_BODY()

public:
	//VARS
	UPROPERTY(SaveGame)
		TArray<FInventoryItem> Inventory;
	UPROPERTY(SaveGame)
		int32 InventorySize;

	UFUNCTION(BlueprintCallable)
		TArray<FInventoryItem> GetInventory() { return Inventory; };

	UFUNCTION(BlueprintCallable)
		int32 GetInventorySize() { return InventorySize; };

	UFUNCTION(BlueprintCallable)
		bool RemoveItem(FInventoryItem Item);
	
	UFUNCTION(BlueprintCallable)
		bool AddItem(FInventoryItem Item);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (CompactNodeTitle))
		int GetIndex(FInventoryItem Item);

	UFUNCTION(BlueprintCallable)
		bool CombineItems(TArray<FInventoryItem> ItemsToCombine, TArray<FInventoryItem> ItemsResult);
	
	UFUNCTION(BlueprintCallable)
		bool DropItem(FInventoryItem ItemToDrop);

	UFUNCTION(BlueprintCallable)
		bool UseItem(FInventoryItem ItemToUse);

	UFUNCTION(BlueprintCallable)
		bool PickUpItem(AKizuItem* ItemToPick);

	UFUNCTION(BlueprintCallable)
		AKizuItem* FindCloseItem();

	UFUNCTION(BlueprintCallable)
		bool FindAndPickItem();
};
