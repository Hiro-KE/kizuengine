// Fill out your copyright notice in the Description page of Project Settings.

//This mostly holds the data

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/ActorComponent.h"
#include "Engine/Engine.h"
#include "KizuTypes.h"
#include "KizuMainComponent.generated.h"

/** The stat struct containing the data for the combat */


UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class KIZUENGINE_API UKizuMainComponent : public UActorComponent, public IKizuTypes
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UKizuMainComponent();
	
	//Variables
	UPROPERTY(BlueprintReadWrite, SaveGame)
		FCombatData CombatData;
	UPROPERTY(BlueprintReadWrite, SaveGame)
		FCombatAnimSet CombatAnimSetData;
	UPROPERTY(VisibleAnywhere, SaveGame)
		TArray<FSpell> SpellData;
	//UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	//	TArray<FObjectives_Ability> AbilityObjectives;


	//References Variables
	UPROPERTY(VisibleAnywhere, category = "References", meta = (AllowPrivateAccess = "true"), SaveGame)
		APawn *PawnRef;
	UPROPERTY(VisibleAnywhere, category = "References", meta = (AllowPrivateAccess = "true"), SaveGame)
		ACharacter *CharRef;
	UPROPERTY(VisibleAnywhere, category = "References", meta = (AllowPrivateAccess = "true"), SaveGame)
		USkeletalMeshComponent *Mesh;
	UPROPERTY(VisibleAnywhere, category = "References", meta = (AllowPrivateAccess = "true"), SaveGame)
		UAnimInstance *AnimInstance;



protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void SetReferences();
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void PlayAnimation(UAnimMontage * Animation, float PlayRate = 1.f);
		
	/** Objectives START */

	void InitializeObjectives();

	//Finish an Ability Objective by passing the objective name
	UFUNCTION(BlueprintCallable, Category = "Objectives|Abilities")
		void FinishObjectiveForAbility(FName ObjectiveName);

	//UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Objectives|Abilities")
	//	void OnObjectiveUpdate(const FObjectives& Objective);
	//virtual void OnObjectiveUpdate_Native(const FObjectives& Objective);

	/** Objectives END */


};
