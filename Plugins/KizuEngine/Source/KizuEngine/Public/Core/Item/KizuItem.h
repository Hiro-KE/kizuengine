// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "KizuTypes.h"
#include "Components/SphereComponent.h"
//#include "Engine/Engine.h"
#include "KizuItem.generated.h"

UCLASS()
class KIZUENGINE_API AKizuItem : public AActor, public IKizuTypes
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AKizuItem();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USkeletalMeshComponent* Mesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USphereComponent* CollisionSphere;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FInventoryItem ItemData;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//ConstructionScript
	virtual void OnConstruction(const FTransform &Transform);
	
	void Initialize(FInventoryItem Data);


	FInventoryItem GetData() {
		return ItemData;
	}
};
