// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/Engine.h"
#include "KizuItem.h"
#include "KizuWeapon.generated.h"


UCLASS()
class KIZUENGINE_API AKizuWeapon : public AKizuItem
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AKizuWeapon();

	//COMPONENTS

	//Variables
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(ExposeOnSpawn=true))
		FWeaponData WeaponData;
	UPROPERTY(BlueprintReadOnly)
		TArray<AActor*> ActorToHitList;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	
	
	//Functions
	


};
