// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Perception/PawnSensingComponent.h"
#include "CharacterDataAsset.h"
#include "KizuCharacter.generated.h"

class UCharacterDataAsset;
class UKizuCombatComponent;
class UKizuInventoryComponent;
class UKizuWidget;
class UWidgetComponent;
class UKizuObjective;

UCLASS()
class KIZUENGINE_API AKizuCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AKizuCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	/** Character DataAsset */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, SaveGame)
		UCharacterDataAsset *CharacterDataAsset;
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), SaveGame)
		UKizuInventoryComponent* InventoryComponent;
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), SaveGame)
		UKizuCombatComponent* CombatComponent;


	/** Ai */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, SaveGame)
		APawn* PawnSensed;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, SaveGame)
		UPawnSensingComponent* PawnSensing;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (DisplayName = "Can See Pawn"), SaveGame)
		uint8 bCanSeePawn : 1;

	/** Objectives */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, SaveGame)
		TArray<TSubclassOf<UKizuObjective>> ObjectiveClassList;
	UPROPERTY(BlueprintReadWrite, SaveGame)
		TArray<UKizuObjective*> ObjectiveList;



	/** Returns Combat Component**/
	class UKizuCombatComponent* GetCombatComponent() const { return CombatComponent; }
	/** Returns Inventory Component**/
	class UKizuInventoryComponent* GetInventoryComponent() const { return InventoryComponent; }
	/** Returns Data Asset **/
	class UCharacterDataAsset* GetDataAsset() const { return CharacterDataAsset;  }

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Taking Damage
	virtual float TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	

	/*Init START*/
	//Initialize The Ai data of the character from the Given Data Asset on creation
	UFUNCTION(BlueprintCallable)
		void InitializeAiData();
	//Initialize The Inventory data of the character from the Given Data Asset on creation
	UFUNCTION(BlueprintCallable)
		void InitializeInventoryData();
	//Initialize The Combat data of the character from the Given Data Asset on creation
	UFUNCTION(BlueprintCallable)
		void InitializeCombatData();
	//Initialize all the data of the character from the Given Data Asset on creation
	UFUNCTION(BlueprintCallable)
		void InitiateCharacter();
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void OnFinishInitialization();
		virtual void OnFinishInitialization_Native();
	/*Init END*/


	/**
	 * Use this in order to give to this HUD a Character to get the data from
	 * @Param WidgetClass : Class of the widget that will be constructed
	 * @Param Character : Character to get the data from
	 */
	UFUNCTION(BlueprintCallable, Category = "UI")
		void InitializeWidget(UWidgetComponent* WidgetComponent, TSubclassOf<UKizuWidget> WidgetClass, AKizuCharacter* Character);
	/**
	 * Use this in order to give to this HUD this Character to get the data from
	 * @Param WidgetClass : Class of the widget that will be constructed
	 */
	UFUNCTION(BlueprintCallable, Category = "UI", meta = (DisplayName = "Initialize Widget For This Character"))
		void InitializeWidget_ThisCharacter(UWidgetComponent* WidgetComponent, TSubclassOf<UKizuWidget> WidgetClass);


	//Pawn Sensing

	UFUNCTION()
		void OnHearNoise(APawn *OtherActor, const FVector &Location, float Volume);

	UFUNCTION()
		void OnSeePawn(APawn *OtherPawn);

	UFUNCTION()
		void DelayEnableSeePawn();

	/** Sets an Objective as done with a given Name. */
	UFUNCTION(BlueprintCallable)
		void SetTaskAsDone(FName Name);
	UFUNCTION(BlueprintImplementableEvent)
		void OnObjectiveDone(UKizuObjective* &Objective);
		void OnObjectiveDone_Native(UKizuObjective* &Objective);

};
