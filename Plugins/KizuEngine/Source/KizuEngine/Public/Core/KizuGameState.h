// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "SpellDataAsset.h"
#include "ItemDataAsset.h"
#include "KizuGameState.generated.h"

/**
 * 
 */
UCLASS()
class AKizuGameState : public AGameStateBase
{
	GENERATED_BODY()

public:

	AKizuGameState();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		USpellDataAsset* SpellDataAsset;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UItemDataAsset* ItemDataAsset;
};
