// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "KizuPlayerController.generated.h"

class ULevelSequencePlayer;
class ULevelSequence;
/**
 * 
 */
UCLASS()
class KIZUENGINE_API AKizuPlayerController : public APlayerController
{
	GENERATED_BODY()
public:

	/** Sequence */
	UPROPERTY()
		ULevelSequencePlayer* LevelSequencePlayer;

public:
	UFUNCTION(BlueprintCallable)
		void KizuPossess(APawn* NewPawn);
	UFUNCTION(BlueprintImplementableEvent)
		void OnEndKizuPosses(bool bSuccess = false);

	/** Sequence */
	UFUNCTION(BlueprintCallable)
		void PlaySequence(ULevelSequence* SequencetoPlay);
};
