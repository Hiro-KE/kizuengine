// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Core/KizuTypes.h"
#include "Components/SphereComponent.h"
#include "Components/AudioComponent.h"
#include "Component/KizuCombatComponent.h"
#include "Engine/Classes/Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

#include "KizuSpell.generated.h"


class USphereComponent;
class UProjectileMovementComponent;

UCLASS()
class KIZUENGINE_API AKizuSpell : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AKizuSpell();





public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
		FSpell SpellData;
	UPROPERTY (VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		USphereComponent* SphereComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		UProjectileMovementComponent* ProjectileMovement;
	UPROPERTY(VisibleAnywhere)
		UKizuCombatComponent* OwnerCombatComponent;


	//TemporaryElements
	UPROPERTY(EditAnywhere)
		UParticleSystemComponent* ParticleSystem_TempStart;
	UPROPERTY(EditAnywhere)
		UParticleSystemComponent* ParticleSystem_TempLoop;
	UPROPERTY(EditAnywhere)
		UParticleSystemComponent* ParticleSystem_TempEnd;
	UPROPERTY(EditAnywhere)
		UAudioComponent* Sound_TempStart;
	UPROPERTY(EditAnywhere)
		UAudioComponent* Sound_TempLoop;
	UPROPERTY(EditAnywhere)
		UAudioComponent* Sound_TempEnd;



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:
	UFUNCTION(BlueprintGetter, BlueprintPure)
		FSpell GetSpellData() { return SpellData; }

	UFUNCTION(BlueprintCallable)
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION(BlueprintCallable)
		void StartSpell();

	UFUNCTION(BlueprintCallable)
		void EndSpell();

	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	//Setter
	void SetOwnerCombatComponent(UKizuCombatComponent* CombatComp) {
		OwnerCombatComponent = CombatComp;
	}

};
