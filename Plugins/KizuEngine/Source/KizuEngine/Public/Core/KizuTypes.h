// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Engine/Engine.h"
#include "KizuGlobalFunctionLibrary.h"
#include "KizuTypes.generated.h"

class ULevelSequence;
class UKizuMainComponent;

/**
 * ENUMS
 */
UENUM(BlueprintType, Category = "Stats")
enum EEffectorType
{
	Health UMETA(DisplayName = "Health"),
	Mana UMETA(DisplayName = "Mana"),
	Energy UMETA(DisplayName = "Energy")
};

UENUM(BlueprintType, Category = "Damagetype")
enum EDamageNature
{
	Pure UMETA(DisplayName = "Pure"),
	Physical UMETA(DisplayName = "Physical"),
	GeneralMagic UMETA(DisplayName = "General Magic"),
	FireMagic UMETA(DisplayName = "Fire Magic"),
	FrostMagic UMETA(DisplayName = "Frost Magic"),
	DarkMagic UMETA(DisplayName = "Dark Magic"),
	NatureMagic UMETA(DisplayName = "Nature Magic"),
	HolyMagic UMETA(DisplayName = "Holy Magic"),
};

UENUM(BlueprintType, Category = "Inventory")
enum EItemType
{
	Weapon UMETA(DisplayName = "Weapon"),
	Consummable UMETA(DisplayName = "Consummable"),
	Armor UMETA(DisplayName = "Armor"),
	Material UMETA(DisplayName = "Material")
};

UENUM(BlueprintType, Category = "CombatState")
enum ECombatState
{
	Idle UMETA(DisplayName = "Idle"),
	Attacking UMETA(DisplayName = "Attacking"),
	Attacked UMETA(DisplayName = "Attacked"),
	Guarding UMETA(DisplayName = "Guarding"),
	Dodging UMETA(DisplayName = "Dodging"),
	Dead UMETA(DisplayName = "Dead")
};

UENUM(BlueprintType, Category = "WeaponType")
enum EWeaponType {
	DefaultWeapon UMETA(DisplayName = "Default"),
	Fist UMETA(DisplayName = "Fist"),
	Sword1H UMETA(DisplayName = "Sword One Handed"),
	Sword2H UMETA(DisplayName = "Sword Two Handed"),
	SwordAndShield UMETA(DisplayName = "Sword And Shield"),
	Staff UMETA(DisplayName = "Staff"),
	Orb UMETA(DisplayName = "Orb")
};

UENUM(BlueprintType, Category = "Direction")
enum EDirection {
	FrontSide UMETA(DisplayName = "Front"),
	BackSide UMETA(DisplayName = "Back"),
	RightSide UMETA(DisplayName = "Right"),
	LeftSide UMETA(DisplayName = "Left")
};

UENUM(BlueprintType, Category = "Character")
enum ECharacter {
	PlayableCharacter UMETA(DisplayName = "Playable"),
	NPC UMETA(DisplayName = "NPC")
};

UENUM(BlueprintType, Category = "Spell Type")
enum ESpellType {
	Projectile UMETA(DisplayName = "Projectile"),
	HomingProjectile UMETA(DisplayName = "Homing Projectile"),
	BouncingProjectile UMETA(DisplayName = "Bouncing Projectile"),
	Instant UMETA(DisplayName = "Instant"),
	Custom UMETA(DisplayName = "Custom Spell")
};

UENUM(BlueprintType, Category = "Combo")
enum EComboState {
	NextCombo UMETA(DisplayName = "NextCombo"),
	ResetCombo UMETA(DisplayName = "ResetCombo")
};

UENUM(BlueprintType, Category = "Faction")
enum EFaction {
	Ally UMETA(DisplayName = "Ally"),
	Enemy UMETA(DisplayName = "Enemy"),
	Neutral UMETA(DisplayName = "Neutral")
};

UENUM(BlueprintType, Category = "Stats")
enum EValueChangesType {
	Add UMETA(DisplayName = "Add"),
	Substract UMETA(DisplayName = "Substract")
};


/**
 * STRUCTS
 */

USTRUCT(BlueprintType)
struct FRegeneration {
	GENERATED_USTRUCT_BODY();
	TEnumAsByte<EEffectorType> ValueType;
	float ValuePerSecond = 1;
};

USTRUCT(BlueprintType)
struct FAbility
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName AbilityID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* Icon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UAnimMontage* MontageToPlay;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool Unlocked = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Cooldown = 3.f;

	void ToLock() {
		Unlocked = false;
	}
	void ToUnlock() {
		Unlocked = true;
	}
};

USTRUCT(BlueprintType)
struct FDamageNature
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct", SaveGame)
		TEnumAsByte<EDamageNature> DamageType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct", SaveGame)
		float Value = 20;

	FORCEINLINE friend FArchive& operator<<(FArchive &Ar, FDamageNature& DamageNature) {
		Ar << DamageNature.DamageType;
		Ar << DamageNature.Value;
	}
};

USTRUCT(BlueprintType)
struct FSpellEffect
{
	GENERATED_USTRUCT_BODY();

	//SPELL EFFECTS (DATA)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		TEnumAsByte<EValueChangesType> ValueChangesType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		TEnumAsByte<EEffectorType> Effect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float Amount;

	bool IsHealthEffect = (Effect == EEffectorType::Health);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (EditCondition = "IsHealthEffect"))
		TEnumAsByte<EDamageNature> Nature;


};

USTRUCT(BlueprintType)
struct FSpell
{
	GENERATED_USTRUCT_BODY();

	
	//GLOBAL
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Main")
		FName Name = "Spell_Name";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Main")
		TEnumAsByte<EFaction> FactionToAffect;
	
	//MOVEMENT
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		TEnumAsByte<ESpellType> SpellType;
	bool IsInstant = (SpellType != ESpellType::Instant);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement", meta = (EditCondition = "(SpellType != ESpellType::Instant)"))
		float ProjectileSpeed = 1000;

	bool IsCustom = (SpellType != ESpellType::Custom);
	UPROPERTY(EditAnywhere, BlueprintReadWrite ,Category = "Custom Choice", Meta = (EditCondition = "(SpellType == ESpellType::Custom)"))
		TSubclassOf<AActor> CustomSpell;

	//DATA
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Values")
		float SpellRadius = 20;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Values")
		TArray<FSpellEffect> SpellEffects;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Consumption")
		TArray<FSpellEffect> SpellConsumption;


	//PARTICLE SYSTEM
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Particle System")
		UParticleSystem* ParticleSystem_Start;
	//SOCKET
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Socket")
		FName CastingSocket = "Root";
	//PARTICLE SYSTEM
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Particle System")
		UParticleSystem* ParticleSystem_Loop;
	//SOCKET
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Socket", meta =(DisplayName = "Target Socket (if homing or attached)"))
		FName TargetSocket = "Root";
	//PARTICLE SYSTEM
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Particle System")
		UParticleSystem* ParticleSystem_End;


	//SOUND
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound Effects")
		USoundBase *Sound_Start;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound Effects")
		USoundBase *Sound_Loop;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound Effects")
		USoundBase *Sound_End;

	//LIFE SPAN
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LifeSpan")
		float LifeSpan = 10.f;
};

USTRUCT(BlueprintType)
struct FEffectorData
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect Data")
		TEnumAsByte<EValueChangesType> ValueChangesType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect Data")
		TEnumAsByte<EEffectorType> Effector;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect Data")
		float Amount = 20;
};

USTRUCT(BlueprintType)
struct FConsumable {
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FEffectorData> Effectors;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Duration;
};

USTRUCT(BlueprintType)
struct FArmor {
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FDamageNature> ArmorValue;
};

USTRUCT(BlueprintType)
struct FWeapon {
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FDamageNature> DamageValue;
};

USTRUCT(BlueprintType)
struct FInventoryItem
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory Struct")
		FName Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory Struct")
		USkeletalMesh * Mesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory Struct")
		FText Description;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* Icon;

	bool operator==(const FInventoryItem& TempItem) {
		return Name == TempItem.Name;
	}
	bool IsValid() {
		return Name != "NullName";
	}
	FInventoryItem() {
		Name = "NullName";
	}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Overrides)
		TEnumAsByte<EItemType> ItemType;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, AdvancedDisplay, Category = "Data Per Type")
		FConsumable ConsumableData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, AdvancedDisplay, Category = "Data Per Type")
		FWeapon WeaponData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, AdvancedDisplay, Category = "Data Per Type")
		FArmor ArmorData;
};

USTRUCT(BlueprintType)
struct FObjectData {
	GENERATED_USTRUCT_BODY();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TArray<FEffectorData> Effector;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Duration;
};

USTRUCT(BlueprintType)
struct FCombatData
{
	GENERATED_USTRUCT_BODY();


	//HP
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct", SaveGame)
		float MaxHP = 200;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct", SaveGame)
		float CurrentHP = 200;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct", SaveGame)
		float RegenHP = 2;

	//MANA
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct", SaveGame)
		float MaxMana = 200;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct", SaveGame)
		float CurrentMana = 200;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct", SaveGame)
		float RegenMana = 5;

	//ENERGY
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct", SaveGame)
		float MaxEnergy = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct", SaveGame)
		float CurrentEnergy = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct", SaveGame)
		float RegenEnergy = 5;

	//Resistances
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct", SaveGame)
		TArray<FDamageNature> Damage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct", SaveGame)
		TArray<FDamageNature> Resistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct", SaveGame)
		uint8 FactionNumber = 0;

	FORCEINLINE friend FArchive& operator<<(FArchive &Ar, FCombatData& CombatData) {
		Ar << CombatData.MaxHP;
		Ar << CombatData.CurrentHP;
		Ar << CombatData.RegenHP;

		Ar << CombatData.MaxMana;
		Ar << CombatData.CurrentMana;
		Ar << CombatData.RegenMana;

		Ar << CombatData.MaxEnergy;
		Ar << CombatData.CurrentEnergy;
		Ar << CombatData.RegenEnergy;

		Ar << CombatData.Damage;
		Ar << CombatData.Resistance;
		Ar << CombatData.FactionNumber;
	}
};

USTRUCT(BlueprintType)
struct FWeaponData {
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		FName ID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		TEnumAsByte<EWeaponType> WeaponType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		float Damage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		UParticleSystem *ParticleOnHit;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		USoundBase *SoundOnHit;

};

USTRUCT(BlueprintType)
struct FHitReact {
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		TEnumAsByte<EDirection> Direction;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		UAnimMontage* Animation;
};

USTRUCT(BlueprintType)
struct FDodgeData {
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		TEnumAsByte<EDirection> DodgingDirection;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		UAnimMontage* MontageToPlay;
};

USTRUCT(BlueprintType)
struct FCombatAnimSet {
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		FName ID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		TEnumAsByte<EWeaponType> WeaponType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		TArray<UAnimMontage*> ComboMontages;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		TArray<FAbility> Abilities;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		TArray<FDodgeData> Dodges;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		TArray<FHitReact> HitReacts;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		UAnimMontage* SheathMontage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		UAnimMontage* UnsheathMontage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		TArray<UAnimMontage*> DeathAnimations;
};




/**
 * CLASS
 */

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UKizuTypes : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class KIZUENGINE_API IKizuTypes
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	
};
