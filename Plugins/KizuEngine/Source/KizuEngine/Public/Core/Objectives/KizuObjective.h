// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "FunctionLibrary/KizuGlobalFunctionLibrary.h"

#include "KizuObjective.generated.h"

/**
 * 
 */

class UTexture2D;
class ULevelSequence;
class AKizuCharacter;


USTRUCT(BlueprintType)
struct FObjectiveTask {
	GENERATED_USTRUCT_BODY();

	FObjectiveTask() {
		TaskName = "None";
	}
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		FName TaskName = "None";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		uint8 bIsDone : 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		FString Description = "";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		UTexture2D* Image;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		FVector WorldPosition = FVector::ZeroVector;

	void SetIsDone(bool NewIsDoneValue) {
		this->bIsDone = NewIsDoneValue;
	}
	void SetObjectiveName(FName NewObjectiveName) {
		this->TaskName = NewObjectiveName;
	}
	bool IsEqual(FObjectiveTask &OtherObjective) {
		return (TaskName.IsEqual(OtherObjective.TaskName));
	}
	bool IsValid() {
		return !(TaskName == "None");
	}
};

USTRUCT(BlueprintType)
struct FObjectiveReward {
	GENERATED_USTRUCT_BODY();

	FObjectiveReward() {
		RewardName = "None";
	}
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		FName RewardName = "None";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		FString Description = "";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		UTexture2D* Image;
};

USTRUCT(BlueprintType)
struct FObjectiveStruct {
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		FName ObjectiveName = "None";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		TArray <FObjectiveTask> TasksList;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		uint8 bIsDone : 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		FString Description = "";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		UTexture2D* Image;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		ULevelSequence* SequenceToPlay;

	bool SetTaskDoneByName(FName TaskName) {
		bool bHasChanged = false;
		for (FObjectiveTask &CurrentTask : TasksList) {
			if (CurrentTask.TaskName == TaskName) {
				CurrentTask.SetIsDone(true);
				bHasChanged = true;
			}
		}

		return bHasChanged;
	}

	//Checks and Sets if the Objective was fully accomplished
	bool ObjectiveUpdate() {
		bool IsFullyDone = true;
		for (FObjectiveTask CurrentTask : TasksList) {
			if (CurrentTask.bIsDone == false) {
				IsFullyDone = false;
				return false;
			}
		}
		bIsDone = IsFullyDone;
		return IsFullyDone;
	}
};


UCLASS()
class KIZUENGINE_API UKizuObjective : public UObject
{
	GENERATED_BODY()


public:



	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FObjectiveStruct ObjectiveDetails;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		AKizuCharacter* Owner;


	
	UFUNCTION(BlueprintCallable)
		void SetOwner(AKizuCharacter* NewOwner) { Owner = NewOwner; }
	UFUNCTION(BlueprintCallable, BlueprintPure)
		AKizuCharacter* GetOwner() { return Owner; }



	UFUNCTION(BlueprintCallable)
		bool SetTaskDone(FName TaskName);

	UFUNCTION(BlueprintImplementableEvent)
		void OnTaskUpdate(FName& TaskName);
	virtual void OnTaskUpdate_Native(FName& TaskName);

	UFUNCTION(BlueprintImplementableEvent)
		void OnObjectiveDone();
	virtual void OnObjectiveDone_Native();
};
