// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Core/KizuTypes.h"
#include "FunctionLibrary/ObjectivesFunctionLibrary.h"
#include "AbilityObjectivesDataAsset.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UAbilityObjectivesDataAsset : public UDataAsset
{
	GENERATED_BODY()
	
public:

	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//	TArray<FObjectives_Ability> AbilityObjectives;
	//

	//UFUNCTION(BlueprintCallable, meta = (DisplayName = "Finish Objective in DataAsset"))
	//	void FinishObjective_local(FName ObjectiveName) {
	//		UObjectivesFunctionLibrary::FinishObjectiveByArray(ObjectiveName, AbilityObjectives);
	//	}

};
