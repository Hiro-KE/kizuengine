// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Runtime/AiModule/Classes/BehaviorTree/BehaviorTree.h"
#include "AiDataAsset.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UAiDataAsset : public UDataAsset
{
	GENERATED_BODY()
	
public:
	
	UPROPERTY(EditAnywhere)
		UBehaviorTree* BehaviorTree;
	UPROPERTY(EditAnywhere)
		float SightRadius = 500.f;
	UPROPERTY(EditAnywhere)
		float LoseSightRadius = 500.f;
	UPROPERTY(EditAnywhere)
		float VisionAngleDegree = 200.f;
	UPROPERTY(EditAnywhere)
		float HearingDistance = 500.f;
	/** For how long does the Ai keep remembering the Ai after seeing him.
	* Lowering the value will make the Ai more aware of other pawns presence whenever they go in range.
	* Increasing the value will make the Ai more aware of the presence of the pawn that he already sensed. (Remembering for longer)
	* Lowering the value will consume more CPU.
	*/
	UPROPERTY(EditAnywhere, AdvancedDisplay)
		float PawnSensingMemoryInterval = 5.f;
	/** Accuracy of sensing. As known as the ticking rate of the sensor. */
	UPROPERTY(EditAnywhere, AdvancedDisplay)
		float SensingInterval = 0.5;
	/** This will cause the BehaviorTree to be re-set on each possess of the AiController. If your Ai will control multiple Characters, then leave it as true*/
	UPROPERTY(EditAnywhere, AdvancedDisplay)
		bool ResetOnEachPossess = true;
};
