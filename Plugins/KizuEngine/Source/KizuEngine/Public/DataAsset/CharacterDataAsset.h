// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "KizuTypes.h"
#include "CombatDataAsset.h"
#include "AiDataAsset.h"
#include "CharacterDataAsset.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UCharacterDataAsset : public UDataAsset
{
	GENERATED_BODY()
	
	
	public:
		UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Combat")
			bool CombatComponent;
		UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (EditCondition = CombatComponent), Category = "Combat")
			UCombatDataAsset *CombatDataAsset;
		UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Inventory")
			bool InventoryComponent;
		UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (EditCondition = InventoryComponent), Category = "Inventory")
			int InventorySize = 50;
		UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Type", meta=(DisplayName = "Can be an NPC?"))
			bool IsNpc;
		UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Type", meta = (EditCondition = IsNpc))
			UAiDataAsset* AiDataAsset;
			/*TEnumAsByte<ECharacter> CharacterType;*/

};
