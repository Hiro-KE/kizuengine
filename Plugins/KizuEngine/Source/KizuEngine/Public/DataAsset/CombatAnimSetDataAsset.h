// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "KizuTypes.h"
#include "CombatAnimSetDataAsset.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UCombatAnimSetDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:
		UPROPERTY(EditAnywhere)
		FCombatAnimSet CombatAnimSet;
	
	
};
