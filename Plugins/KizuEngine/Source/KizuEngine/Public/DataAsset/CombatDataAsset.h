// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "KizuTypes.h"
#include "SpellDataAsset.h"
#include "DataAsset//AbilityObjectivesDataAsset.h"
#include "CombatAnimSetDataAsset.h"
#include "CombatDataAsset.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UCombatDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FCombatData CombatData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UCombatAnimSetDataAsset* CombatAnimSet_DataAsset;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Needs Objectives for abilities?"))
		uint8 bNeedsObjectives : 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bNeedsObjectives"))
		UAbilityObjectivesDataAsset* AbilityObjective_DataAsset;

};
