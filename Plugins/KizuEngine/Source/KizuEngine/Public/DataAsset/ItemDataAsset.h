// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "KizuTypes.h"
#include "Engine/Engine.h"
#include "ItemDataAsset.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UItemDataAsset : public UDataAsset, public IKizuTypes
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TArray<FInventoryItem> InventoryItems;


//	
//#if WITH_EDITOR  
//	//WORK IN PROGRESS
//	void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override
//	{
//
//		//Get the name of the property that was changed  
//		FName PropertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;
//		//UE_LOG(LogTemp, Warning, TEXT("VALUE NAME : %s"), *PropertyName.ToString());
//		// We test using GET_MEMBER_NAME_CHECKED so that if someone changes the property name  
//		// in the future this will fail to compile and we can update it.  
//		if ((PropertyName == "ItemType"))
//		{
//			UField* InventoryStruct = PropertyChangedEvent.Property->GetOwnerStruct();
//			//UProperty* ItemProperty = InventoryStruct->FindPropertyByName("ItemType");
//			
//			if (UStructProperty* StructProperty = Cast<UStructProperty>(InventoryStruct)) { //CAST NOT WORKING
//				FInventoryItem *InventoryItem = StructProperty->ContainerPtrToValuePtr<FInventoryItem>(StructProperty);
//				UE_LOG(LogTemp, Warning, TEXT("VALUE NAME : %s"), *InventoryItem->Name.ToString());
//				
//			}
//
//
//
//			//if (ItemProperty) {	// THIS IS WORKING BUT I CAN'T GET THE USTRUCT PROPERTY
//			//	if (auto ItemTypeEnumProperty = Cast<UEnumProperty>(ItemProperty)) {
//			//		EItemType* ItemTypeEnum = ItemTypeEnumProperty->ContainerPtrToValuePtr<EItemType>(ItemTypeEnumProperty);
//			//		//UEnum::GetValueAsString<EItemType>(ItemTypeEnum);
//			//		UE_LOG(LogTemp, Warning, TEXT("VALUE NAME : %s"), *PropertyName.ToString());
//			//		
//			//	}
//			//}
//
//
//			//UScriptStruct *InventoryItemSS = FInventoryItem::StaticStruct();
//			//InventoryItemSS->FindPropertyByName("");
//
//
//			 //CurrentStruct = Cast<FInventoryItem::StaticStruct>(PropertyChangedEvent.Property->GetOwnerStruct());
//
//		}
//
//		// Call the base class version  
//		Super::PostEditChangeProperty(PropertyChangedEvent);
//	}
//#endif
//
};
