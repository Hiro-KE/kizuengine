// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "KizuTypes.h"
#include "SpellDataAsset.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API USpellDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TArray<FSpell> SpellList;
	
	
};
