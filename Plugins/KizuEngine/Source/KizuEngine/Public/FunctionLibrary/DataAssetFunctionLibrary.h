// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/Engine.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "KizuTypes.h"
#include "DataAssetFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UDataAssetFunctionLibrary : public UBlueprintFunctionLibrary, public IKizuTypes
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintPure, BlueprintCallable)
		static FWeaponData GetWeaponFromDataAsset(UDataAsset * DataAsset,FName WeaponID, bool & Found);


		static FSpell GetSpell(UDataAsset * DataAsset, FName SpellID, bool & Found);
	UFUNCTION(BlueprintCallable, BlueprintPure)
		static FSpell GetSpell(TArray<FSpell> SpellList, FName SpellID, bool & Found);
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
		static FAbility GetAbility(TArray<FAbility> AbilityList, FName AbilityID, bool& Found);


		static FInventoryItem GetItem(TArray<FInventoryItem> ItemsList, FName ItemName, bool& Found);
	UFUNCTION(BlueprintCallable, BlueprintPure)
		static FInventoryItem GetItem(UDataAsset* DataAsset, FName ItemName, bool& Found);

	
};
