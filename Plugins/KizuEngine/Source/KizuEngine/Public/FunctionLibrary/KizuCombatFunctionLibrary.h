// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"
#include "Engine/Engine.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "KizuWeapon.h"
#include "KizuSpell.h"
#include "KizuTypes.h"
#include "KizuCombatComponent.h"
#include "KizuCombatFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UKizuCombatFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	

	//GLOBAL
	UFUNCTION(BlueprintPure, BlueprintCallable)
		static UKizuCombatComponent* GetCombat(AActor* TargetActor);


	//COMBO
	UFUNCTION(BlueprintCallable)
		static void PlayCombo(UKizuCombatComponent* CombatComponent, TArray<UAnimMontage*> MontagesToPlay);
	UFUNCTION(BlueprintCallable)
		static void ComboCounter(UKizuCombatComponent * CombatComponent, TEnumAsByte<EComboState> ComboState);
	//SPELL
	UFUNCTION(BlueprintCallable)
		static AKizuSpell* SpawnSpell(AActor* Spawner);
	UFUNCTION(BlueprintCallable)
		static void CastSpell(UKizuCombatComponent* CombatComponent, FName SpellName);
	UFUNCTION(BlueprintCallable)
		static bool Consumption(UKizuCombatComponent* CombatComponent, FSpell SpellData);
	UFUNCTION(BlueprintCallable)
		static void ApplySpellEffects(UKizuCombatComponent* SelfCombatComponent, UKizuCombatComponent* TargetCombatComponent, FSpellEffect SpellEffect);


	//ABILITY
	UFUNCTION()
		static void PlayAbility(UKizuCombatComponent* CombatComponent, FAbility AbilityToPlay);

	//Dodging
	UFUNCTION(BlueprintCallable, BlueprintPure)
		static UAnimMontage* FindDodgeFromAnimSet(bool& Found, FCombatAnimSet CombatAnimSet, EDirection DodgingDirection = EDirection::FrontSide);


	//HIT REACT
	/**
	 * Returns the Normalization of the orientation of two characters (By getting their MeshComponent)
	 * @param CharacterA The first Character actor
	 * @param CharacterB The second Character actor
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, meta =(CompactNodeTitle))
		static TEnumAsByte<EDirection> GetDirection(ACharacter* CharacterA, ACharacter* CharacterB);
	UFUNCTION(BlueprintCallable)
		static UAnimMontage* GetMontageReact(FCombatAnimSet CombatAnimSet, TEnumAsByte<EDirection> Direction);

	//STATE
	UFUNCTION(BlueprintCallable)
		static bool CheckState(UKizuCombatComponent* CombatComponent, TEnumAsByte<ECombatState> CombatState);

	//FACTION
	UFUNCTION(BlueprintCallable)
		static TEnumAsByte<EFaction> CheckFaction(AActor* ActorA, AActor* ActorB);

	//TARGETTING
	UFUNCTION(BlueprintCallable)
		static bool ToTarget(UKizuCombatComponent* CombatComponent, float TargetRange = 1500);

};
