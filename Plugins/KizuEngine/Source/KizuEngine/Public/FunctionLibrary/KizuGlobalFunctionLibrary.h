// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/Engine.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "KizuGlobalFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class UKizuGlobalFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public: 
	//UFUNCTION(BlueprintCallable, meta = (DisplayName = "Get Forward Tracing", Keywords = "Get Forward Tracing"), Category= "KizuGlobal")
	//static void GetForwardTracing(const AActor *Actor, const float TraceLength, FVector &TraceStart, FVector &TraceEnd);

	UFUNCTION(BlueprintCallable, Category = "KizuGlobal")
		static float AddValue(float Value, float MaxValue, float CurrentValue);
	UFUNCTION(BlueprintCallable, Category = "KizuGlobal")
		static float SubstractValue(float Value, float CurrentValue);
	
	UFUNCTION(BlueprintCallable)
		static void KizuMessage(FString StringToDisplay);

	UFUNCTION(BlueprintCallable)
		static TArray<FHitResult> MultiTrace(AActor* TracingActor, FVector StartTrace, FVector EndTrace, float Width, TArray<AActor*> ToIgnore, bool DrawDebug);

	UFUNCTION(BlueprintCallable)
		static void DebugSphere(AActor* DebuggingActor, FVector SphereLocation, float Width);


	/**
	 *	Getting the Normal of a Location/Forward Vector
	 *  Normalize ( Location - Location + ForwardVector ) 
	 */
	UFUNCTION(BlueprintCallable, meta=(CompactNodeTitle))
		static FVector GetNormalizedOrientation(USceneComponent* SceneComponent);



	template< class T >
	static T* SpawnActor(AActor* Spawner, TSubclassOf<T> ClassToSpawn)
	{
		UWorld* World = Spawner->GetWorld();
		UPROPERTY()
			T* ObjectToSpawn;
		if (World)
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = Spawner;
			FRotator Rotation = Spawner->GetActorRotation();
			FVector Location = Spawner->GetActorLocation();
			ObjectToSpawn = World->SpawnActor<T>(Location, Rotation, SpawnParams);
			
		//	return ObjectToSpawn;
			return  CastChecked<T>(ObjectToSpawn);
		}
		return nullptr;
	}

	//UFUNCTION(BlueprintCallable)
	//	static AActor* SpawnActor(AActor* Spawner, TSubclassOf<AActor> ClassToSpawn);

	//UFUNCTION(BlueprintCallable)
	//	static void AttachToSocket(USkeletalMeshComponent CharacterMesh, AActor ToAttach, FName SocketName);
};

//Conversion from Enum to String
template<typename TEnum>
static FORCEINLINE FString GetEnumValueAsString(const FString& Name, TEnum Value)
{
	const UEnum* enumPtr = FindObject<UEnum>(ANY_PACKAGE, *Name, true);
	if (!enumPtr)
	{
		return FString("Invalid");
	}
	return enumPtr->GetNameByValue((int64)Value).ToString();
}


