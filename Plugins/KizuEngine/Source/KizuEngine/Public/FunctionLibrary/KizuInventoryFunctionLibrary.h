// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "KizuTypes.h"
#include "KizuItem.h"
#include "KizuCombatComponent.h"
#include "KizuInventoryComponent.h"
#include "KizuInventoryFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UKizuInventoryFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintPure, BlueprintCallable)
		static UKizuInventoryComponent* GetInventoryComponent(AActor* TargetActor);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		static TArray<FInventoryItem> GetInventory(AActor* TargetActor, bool& Found);

	UFUNCTION(BlueprintCallable)
		static AKizuItem* SpawnItem(AActor* Spawner, FInventoryItem ItemData);

	UFUNCTION(BlueprintCallable)
		static bool ConsumeItem(FInventoryItem ItemToConsume, UKizuCombatComponent* CombatComponent);

	UFUNCTION(BlueprintCallable)
		static bool EquipWeapon(FInventoryItem ItemToEquip, UKizuCombatComponent* CombatComponent);

	UFUNCTION(BlueprintCallable)
		static bool EquipArmor(FInventoryItem ItemToEquip, UKizuCombatComponent* CombatComponent);

	UFUNCTION(BlueprintCallable)
		static void DestroyItem(AKizuItem* ItemToDestroy);
	
};
