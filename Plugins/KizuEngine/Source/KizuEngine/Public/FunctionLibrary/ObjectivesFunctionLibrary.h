// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Core/KizuTypes.h"
#include "ObjectivesFunctionLibrary.generated.h"

class UKizuMainComponent;
/**
 * 
 */
UCLASS()
class KIZUENGINE_API UObjectivesFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
protected:
	
public:

/*
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Finish Objective"))
		static TArray<FObjectives_Ability> FinishObjectiveByArray(FName ObjectiveName, UPARAM(ref) TArray<FObjectives_Ability> &AbilityObjectivesToFetch);
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Finish Objective And Update"))
		static TArray<FObjectives_Ability> FinishObjectiveByComp(FName ObjectiveName, UKizuMainComponent* MainComponent);
	UFUNCTION(BlueprintCallable, BlueprintGetter, BlueprintPure)
		static FObjectiveBase GetSingleObjective(FName ObjectiveName, UPARAM(ref) TArray<FObjectives_Ability> &AbilityObjectivesToFetch);
	UFUNCTION(BlueprintCallable, BlueprintGetter, BlueprintPure)
		static TArray<FObjectiveBase> GetObjectives(FName ObjectiveName, UPARAM(ref) TArray<FObjectives_Ability> &AbilityObjectivesToFetch);
	UFUNCTION(BlueprintCallable, BlueprintGetter, BlueprintPure)
		static FObjectives_Ability FindObjectivesByAbility(FName AbilityName, UPARAM(ref) TArray<FObjectives_Ability> &AbilityObjectivesToFetch, bool &Found);
	UFUNCTION(BlueprintCallable)
		static FObjectives SetObjectiveDoneByName(FName ObjectiveName, UPARAM(ref) FObjectives &Objective);
	UFUNCTION(BlueprintCallable)
		static void UpdateObjectiveStatus(UKizuMainComponent* MainComponent, TArray<FObjectives_Ability>& NewAbilityObjectives);*/

};
