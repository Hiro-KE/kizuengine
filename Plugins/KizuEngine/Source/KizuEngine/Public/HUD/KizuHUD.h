// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Core/KizuTypes.h"
#include "Core/KizuCharacter.h"
#include "Component/KizuCombatComponent.h"
#include "Component/KizuInventoryComponent.h"
#include "KizuHUD.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API AKizuHUD : public AHUD
{
	GENERATED_BODY()


private :

	//References
	UPROPERTY()
		AKizuCharacter* Character;

	//Components
	UPROPERTY()
		UKizuCombatComponent* CombatComponent;
	UPROPERTY()
		UKizuInventoryComponent* InventoryComponent;

	//Inventory
	UPROPERTY()
		TArray<FInventoryItem> Inventory;

public:
	//Getters
	UFUNCTION(BlueprintPure, BlueprintCallable, meta = (CallableWithoutWorldContext))
		AKizuCharacter* GetCharacter() {
		return (Character ?  Character : nullptr);
	}
	UFUNCTION(BlueprintPure, BlueprintCallable, meta = (CallableWithoutWorldContext))
		UKizuCombatComponent* GetCombatComponent() {
		return (CombatComponent ? CombatComponent : nullptr);
	}
	UFUNCTION(BlueprintPure, BlueprintCallable, meta = (CallableWithoutWorldContext))
		UKizuInventoryComponent* GetInventoryComponent() {
		return (InventoryComponent ? InventoryComponent : nullptr);
	}
	UFUNCTION(BlueprintPure, BlueprintCallable, meta = (CallableWithoutWorldContext))
		TArray<FInventoryItem> GetInventory() {
		UPROPERTY()
			TArray<FInventoryItem> NullInventory;
		return (InventoryComponent ? Inventory : NullInventory);
	}
};
