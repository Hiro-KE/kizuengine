// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HUD/KizuWidget.h"
#include "Core/KizuTypes.h"
#include "KizuItemWidget.generated.h"

/**
 * 
 */
UCLASS()
class KIZUENGINE_API UKizuItemWidget : public UKizuWidget
{
	GENERATED_BODY()
private:
	UPROPERTY()
		FInventoryItem ItemData;

	UPROPERTY()
		UUserWidget* WidgetContainer;

public: 
	UFUNCTION(BlueprintCallable, BlueprintPure)
		FInventoryItem GetItemData() { return ItemData; };
	UFUNCTION(BlueprintCallable)
		void SetItemData(FInventoryItem NewItemData) { ItemData = NewItemData; };
	UFUNCTION(BlueprintCallable, BlueprintPure)
		FText GetItemNameAsText() { return FText::FromName(ItemData.Name); };
	UFUNCTION(BlueprintCallable, BlueprintPure)
		UTexture2D* GetItemIcon() { return ItemData.Icon; };

	UFUNCTION(BlueprintCallable, BlueprintPure)
		UUserWidget* GetWidgetContainer() { return WidgetContainer; };
	UFUNCTION(BlueprintCallable)
		void SetWidgetContainer(UUserWidget* NewWidgetContainer) { WidgetContainer = NewWidgetContainer; };
};
