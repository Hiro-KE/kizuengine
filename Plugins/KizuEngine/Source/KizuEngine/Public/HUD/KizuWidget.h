// Copyright (C) KizuTeam Ltd. 2019. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Component/KizuCombatComponent.h"
#include "Component/KizuInventoryComponent.h"
#include "Core/KizuCharacter.h"
#include "Core/KizuTypes.h"
#include "Components/UniformGridPanel.h"
#include "KizuWidget.generated.h"

class UKizuItemWidget;
/**
 * 
 */
UCLASS()
class KIZUENGINE_API UKizuWidget : public UUserWidget
{
	GENERATED_BODY()
		//UKizuWidget::UKizuWidget();

private:


	UPROPERTY()
		AKizuCharacter* CharacterRef;

	UPROPERTY()
		uint8 bPropertiesSet : 1 ;





public:

	// Leave this as None if you're going to use this Widget for player Data.
	UPROPERTY(BlueprintReadWrite, Category = "Default", meta = (ExposeOnSpawn = "true"))
		AKizuCharacter* OtherCharacter;


	UKizuWidget(const FObjectInitializer& ObjectInitializer);

	// Optionally override the Blueprint "Event Construct" event
	virtual void NativeConstruct() override;

	// Optionally override the tick event
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;



	// Use this instead of OnConstruct events if you're in need for the pre-made functions here that will get the player data.
	UFUNCTION(BlueprintImplementableEvent, BlueprintCosmetic)
		void OnFinishedInitialization();

	//In case you're switching from character and you're using same HUD, maybe calling this would be needed in order to refresh the Data and refer them to your character.
	UFUNCTION(BlueprintCallable)
		void InitializeReferences();

	/*Global START*/
	//Get the character reference
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Global|Data")
		AKizuCharacter* GetCharacter() { return (bPropertiesSet) ? CharacterRef : nullptr; };
	//Set the character and reinitialize the data
	UFUNCTION(BlueprintCallable, Category = "Global|Data")
		void SetCharacter(AKizuCharacter* NewCharacter);
	/*Global END*/


	/** Combat Data START*/
	//Get the Combat Data of the player
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Combat")
		UKizuCombatComponent* GetCombatComponent() { return (bPropertiesSet) ? CharacterRef->GetCombatComponent() : nullptr; };
	//Get the health Ratio of your player Character (Between 0 and 1)
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Combat|Data")
		float GetHealthRatio(){
		return (GetCombatComponent())
			? (GetCombatComponent()->CombatData.CurrentHP / GetCombatComponent()->CombatData.MaxHP)
			: (1.f);
		}
	//Get the Mana Ratio of your player Character (Between 0 and 1)
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Combat|Data")
		float GetManaRatio() {
		return (GetCombatComponent())
			? (GetCombatComponent()->CombatData.CurrentMana / GetCombatComponent()->CombatData.MaxMana)
			: (1.f);
		}
	//Get the Energy Ratio of your player Character (Between 0 and 1)
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Combat|Data")
		float GetEnergyRatio() {
		return (GetCombatComponent())
			? (GetCombatComponent()->CombatData.CurrentEnergy / GetCombatComponent()->CombatData.MaxEnergy)
			: (1.f);
	}
	//Get the Combat Data of your player Character
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Combat|Data")
		FCombatData GetCombatData() {
		UPROPERTY()
		FCombatData InvalidTemp;
		return (GetCombatComponent())
			? (GetCombatComponent()->CombatData)
			: (InvalidTemp);
	}
	/** Combat Data END*/



	/** Inventory Data START */
	//Get the inventory component of the player character
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Inventory")
		UKizuInventoryComponent* GetInventoryComponent() { return (bPropertiesSet) ? (CharacterRef->GetInventoryComponent()) : nullptr; };
	//Get the inventory of the player character
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Inventory|Data")
		TArray<FInventoryItem> GetInventory() { return GetInventoryComponent()->Inventory; };
	/** Inventory Data END */



	//Fill a uniform Grid from the Inventory Data of the Character
	UFUNCTION(BlueprintCallable)
		void FillUniformGridFromInventory(UUniformGridPanel* GridPanel, TSubclassOf<UKizuItemWidget> ItemWidgetClass, int ColumnCount = 4);
};
