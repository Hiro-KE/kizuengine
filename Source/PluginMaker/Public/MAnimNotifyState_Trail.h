// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotifyState_Trail.h"
#include "MAnimNotifyState_Trail.generated.h"

/**
 * 
 */
UCLASS()
class PLUGINMAKER_API UMAnimNotifyState_Trail : public UAnimNotifyState_Trail
{
	GENERATED_BODY()
	
public :
	
	UFUNCTION(BlueprintCallable, Category = "Trail")
		void SetPSTemplate(UParticleSystem* InParticleSystem) {
		if (InParticleSystem) {
			PSTemplate = InParticleSystem;
		}
	}
};
